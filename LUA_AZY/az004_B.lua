do
pin1 = 1
int1 = 1000

pin2 = 2
int2 = 3000

function makeblink(pin)
    gpio.mode(pin, gpio.OUTPUT)
    local ligth = 1
    return function()
        gpio.write(pin, ligth)
        print("Writе  to "..pin.." "..ligth)
        ligth = (ligth == 0) and 1 or 0
    end
end

function maketimer(interval, call)
    tmr.create():alarm(interval, tmr.ALARM_AUTO, call)
end

maketimer(int1, makeblink(pin1))
maketimer(int2, makeblink(pin2))
end