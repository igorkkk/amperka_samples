local w = rtcmem.read32(0)
if w == 701 then
   print("Start Server!")
   dofile("http.lua")
else
print("Try Run main.lua!")
tmr.create():alarm(15000, 0, function()
   if file.exists("main.lua") then
      dofile("main.lua")
   else
       print("No main.lua!")
       rtcmem.write32(0, 701)
       node.restart()
   end
end)
end
