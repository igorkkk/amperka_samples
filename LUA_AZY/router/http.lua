do
	local function executeCode (code)
		local codeOUT
		local oldprint = print
		local newprint = function(...)
			for i,v in ipairs(arg) do
				codeOUT = (codeOUT or "") .. tostring(v)
			end
		end
		print = newprint
		local luaFunc, err = loadstring(code)
		if luaFunc ~= nil then
			local status, err = pcall(assert(luaFunc))
			if status == false then
				print("[Runtime error: "..(err or "#err#").."]")
			end
		else
			print("[Syntax error: "..(err or "#err#").."]")
		end
		print = oldprint
		return codeOUT 
	end

	local function escapeMagic(str)
		return string.gsub(str, "[%(%)%.%+%-%*%?%[%]%^%$%%]", "%%%1")
	end
	local function replace(str, what, with)
		what = string.gsub(what, "[%(%)%.%+%-%*%?%[%]%^%$%%]", "%%%1")
		with = string.gsub(with, "[%%]", "%%%%")
		return string.gsub(str, what, with)
	end

     if srv then srv:close(); srv = nil end 
	srv=net.createServer(net.TCP)

	srv:listen(80,function(conn)
		local EndPOST = "true"
		local StartPOSTEnd = "false"
		local ContentLengthOF = 0
		local method = nil
		local StartPOST = nil

		conn:on("receive", function(conn,request)
			local _, _, method, path, vars = string.find(request, "([A-Z]+) (.+)?(.+) HTTP")
			if(method == nil)then
				_, _, method, path = string.find(request, "([A-Z]+) (.+) HTTP")
			end

			if method == "GET" and EndPOST == "true" then
				print("[GET]")
				if vars ~= nil then 
					for k, v in string.gmatch(vars, "(%w+)=([%w%.~%-_]+)&*") do
						if(k == "command")then 
							if(v == "restart")then
								print("[Restart ESP8266]")
								print("**[Connection closed]")
								rtcmem.write32(0, 0)
                                        node.restart()
							elseif(v == "delfile")then delfile = "true"
							elseif(v == "runfile")then runfile = "true"
							else print("[Unknow incoming command]")
							end
							
						elseif(k == "filename")then
							if(delfile == "true")then 
								if v ~= "init.lua" and v ~= "http.lua" and v ~= "index.htm" then
									delfile = "false" 
									print("[Delete " .. (v or "---") .. " file]")
									file.remove(v)
									delfile = nil
								end
							elseif(runfile == "true")then print("[Running " .. (v or "---") .. " file]"); dofile(v); runfile = nil
							end
							conn:send("HTTP/1.1 204 No Content\r\nConnection: close\r\n\r\n")
						end
					end
				else 
					if(file.exists((path:match("/(.+)") or "index.htm")))then 
						if(path:find("%.htm"))then ContentType="text/html"
						elseif(path:find("%.html"))then ContentType="text/html"
						elseif(path:find("%.css"))then ContentType="text/css"
						elseif(path:find("%.js"))then ContentType="text/javascript"
						elseif(path:find("%.png"))then ContentType="image/png"
						elseif(path:find("%.gif"))then ContentType="image/gif"
						elseif(path:find("%.ico"))then ContentType="image/x-icon"
						elseif(path:find("%.jpg") or path:find("%.jpeg"))then ContentType="image/jpeg"
						else ContentType="text/html"
						end
						requestFile,pos=path:gsub("/(.+)", "%1")
						if(pos == 0 and requestFile == "/")then requestFile="index.htm"; end 
						print("[Sending file " .. (requestFile or "#requestFile#") .. "]")
						filePos=0
						conn:send("HTTP/1.1 200 OK\r\nConnection: close\r\n\r\n")
						ContentType = nil;
					else
						print("[File /" .. (path:gsub("/(.+)", "%1") or "!path:gsub!") .. " not found]")
						conn:send("HTTP/1.1 404 Not Found\r\n\r\n")
						conn:close()
						collectgarbage()
					end
				end
			elseif((method == "POST" or EndPOST == "false") and request ~= nil)then
				if(EndPOST == "true")then print("[POST]") end
				if(ContentLength == nil)then ContentLength = request:match("\r\nContent%-Length:%s(.-)\r\n") end
				if(boundary == nil)then boundary = request:match("boundary=(.-)\r\n") end
				if(filename == nil)then filename = request:match("filename=\"(.-)\"\r\n") end
				if(ContentLength == nil or boundary == nil or filename == nil)then StartPOSTEnd = "false" end

				if(StartPOST == nil and boundary ~= nil and StartPOSTEnd == "false")then
					StartPOST = request:match("(%-%-" .. escapeMagic(boundary) .. ".+\r\n\r\n).+")
					if(StartPOST ~= nil)then
						POST = request:match(escapeMagic(StartPOST) .. "(.+)\r\n%-%-" .. escapeMagic(boundary) .. "%-%-\r\n")
						if(POST ~= nil)then
							if(filename ~= nil and file.open("~" .. filename, "w+"))then
								file.write(POST)
								file.close()
								print("[Upload file " .. filename .. "]")
								print("[" .. #POST .. " bytes left]")
							end
							POST = nil
							file.remove(filename);
							file.rename("~" .. filename, filename)
							boundary = nil
							print("[Upload " .. filename .. " done]")
							filename = nil
							conn:send("HTTP/1.1 204 No Content\r\nConnection: close\r\n\r\n")
							ContentLength = nil
						else
							POST = request:match(escapeMagic(StartPOST) .. "(.+)")
							if(POST ~= nil)then
								if(filename ~= nil and file.open("~" .. filename, "w+"))then
									file.write(POST)
									file.close()
									ContentLengthOF = tonumber(ContentLengthOF) + #boundary + 2 + 2 + 2 + 2 + #StartPOST + #POST - 1
									print("[Upload file " .. filename .. "]")
									print("[" .. ContentLengthOF .. " of " .. ContentLength .. " bytes left]")
								end
								POST = nil
								EndPOST = "false"
								StartPOSTEnd = "true"
								conn:send("HTTP/1.1 100 Continue\r\n\r\n")
								return
							end
						end
						StartPOST = nil
					else
						EndPOST = "false"
					end
				end
				if(EndPOST == "false" and StartPOSTEnd == "true")then
					if((tonumber(ContentLengthOF) + #request) <= tonumber(ContentLength))then
						if(filename ~= nil and file.open("~" .. filename, "a+"))then
							file.write(request)
							file.close()
							ContentLengthOF = tonumber(ContentLengthOF) + #request
							print("[" .. ContentLengthOF .. " of " .. ContentLength .. " bytes left]")
						end
						EndPOST = "false"
					else
						POST = request:sub(0, tonumber(ContentLength) - tonumber(ContentLengthOF))
						if(POST ~= nil)then
							if(filename ~= nil and file.open("~" .. filename, "a+"))then
								file.write(POST)
								file.close()
						 		ContentLengthOF = tonumber(ContentLengthOF) + string.len(request:sub(0, tonumber(ContentLength) - tonumber(ContentLengthOF)))
								print("[" .. ContentLengthOF .. " of " .. ContentLength .. " bytes left]")
							end
							POST = nil
							file.remove(filename)
							file.rename("~" .. filename, filename)
							boundary = nil
							print("[Upload " .. filename .. " done]")
							filename = nil
							conn:send("HTTP/1.1 204 No Content\r\nConnection: close\r\n\r\n")
							ContentLength = nil
						end
					end
				end
			else
				print("[Unknow data]")
			end
			collectgarbage()
		end)
		conn:on("sent",function(conn)
			if(requestFile ~= nil)then
				if(file.open(requestFile,r))then
					file.seek("set",filePos)
					local partial_data = file.read(1536)
					file.close()
					if(partial_data)then 
						filePos = filePos + #partial_data
						local partial_dataLen = #partial_data
						for S in partial_data:gmatch("<%?lua(.-)%?>") do 
							partial_data = partial_data:gsub("<%?lua" .. escapeMagic(S) .. "%?>",(executeCode(S) or ""))
						end;
						if(partial_data:find("<%?lua"))then 
							if(file.open(requestFile,r))then 
								local PosSeek = 64
								while not partial_data:find("%?>") do
									file.seek("set",filePos)
									partial_data = partial_data .. file.read(PosSeek)
									filePos = filePos + PosSeek
								end
								PosSeek = nil
								file.close()
								local FindStrBegin, FindStrEnd = partial_data:find("(.+)%?>")
								filePos = filePos - (#partial_data - FindStrEnd)
								partial_data = (partial_data:match("(.+)%?>") or "").."?>"
								for S in partial_data:gmatch("<%?lua(.-)%?>") do
									partial_data = partial_data:gsub("<%?lua" .. escapeMagic(S) .. "%?>",(executeCode(S) or ""))
								end;
								FindStrBegin = nil
								FindStrEnd = nil
							end
						end
						if(partial_data ~= "" and partial_data ~= nil)then 
							conn:send((partial_data or ""))
							partial_data = nil;
							print("["..(filePos or "#filePos#").." bytes sent]")
						else
							print("[NULL DATA TO SENT!]")
						end
						if(partial_dataLen == 1536)then 
							ConnStatus = true
							return
						else
							print("*[Connection closed]")
							ConnStatus = conn:close()
						end
					end
				else
					print("[Error opening file "..(requestFile or "#requestFile#").."]")
				end
			end
			if(ConnStatus == true)then
				ConnStatus = conn:close()
				print("[Connection closed]")
			end
		end)
		conn:on("disconnection", function(conn, payload)
			print("[The client broke the connection to the server] [" .. payload .. "]")
			EndPOST = "true"
			StartPOSTEnd = "false"
			ContentLengthOF = 0
			collectgarbage()
		end)
		conn:on("reconnection", function(conn, payload)
			print("[net.TCP server reconnection] [" .. payload .. "]")
			EndPOST = "true"
			StartPOSTEnd = "false"
			ContentLengthOF = 0
			collectgarbage()
		end)
	end)
end