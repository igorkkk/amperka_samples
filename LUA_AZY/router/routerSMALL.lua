routerPin = 4
lightPin = 3

gpio.mode(routerPin,gpio.OUTPUT)
gpio.mode(lightPin,gpio.OUTPUT)
gpio.write(lightPin,gpio.LOW)

trying = 14
restarting = 0
checkperiod = 30000

function check()
	checkTMR:stop()
	local servrs = {"www.google.com", "ya.ru", "www.yahoo.com", "www.mail.ru"}
    local servNo = 1
	
	local function isconnect(srv)
		net.dns.resolve(srv, function(sk, ip)
			if (ip == nil) or (ip == "10.0.0.1") then 
				print("DNS "..servrs[servNo].." fail!")
				if servNo < #servrs then
					servNo = servNo + 1
					isconnect(servrs[servNo])
				else
					checkTMR:start()
				end
			else 
				trying, restarting = 3, 0
				checkTMR:start()
				print("Got IP: "..ip.." at "..servrs[servNo] )
			end
		end)
	end
	isconnect(servrs[1])
end

function restartRouter()
	checkTMR:stop()
	local function relay(muv)
		if muv then
			gpio.write(routerPin,gpio.LOW)
			gpio.write(lightPin,gpio.HIGH)
		else
			gpio.write(routerPin,gpio.HIGH)
			gpio.write(lightPin,gpio.LOW)
		end
	end
	print("Restart Router!")
	relay(true)
	local stoprelay = tmr.create()
	stoprelay:alarm(15000, 0, function() 
        relay(false)
        print("Exit Restart Router!")
		tmr.unregister(stoprelay)
		stoprelay = nil
		countall()
		checkTMR:start()
	end)
end

function countall()
	restarting = restarting + 1
    print("Restart No "..restarting)
	if restarting == 1 then
        trying = 14
	elseif restarting == 2 then
        trying = 28
    else
        trying = 120
    end
end

checkTMR = tmr.create()
checkTMR:register(checkperiod, 1, function()
    print("Heap now: "..node.heap())
	local ip = wifi.sta.getip()
    print("Trying: "..trying.." and IP:", ip)
    if (not ip and trying <= 2) then
        restartRouter()
    elseif trying <= 0  then
        restartRouter()
    elseif ip then
        print("Check connection")
        check()
    end
	trying = trying - 1
end)
checkTMR:start()
