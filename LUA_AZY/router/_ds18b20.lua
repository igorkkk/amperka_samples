local M={}
M.adrtbl = {}
M.pin = false

function M.addrs()
    ow.setup(M.pin)
    ow.reset_search(M.pin)
    local adr
    repeat
        adr = ow.search(M.pin)
        if(adr ~= nil) then
            table.insert(M.adrtbl, adr)
        end
    until (adr == nil)
    ow.reset_search(M.pin)
    adr = nil
    M.askT()
end

function M.askT()
    ow.setup(M.pin)
    for _, v 
in pairs(M.adrtbl) do
        ow.reset(M.pin)
        ow.select(M.pin, v)
        ow.write(M.pin, 0x44, 1)
    end
    v = nil
end

function M.readResult(temptabl)    
    local data
    local crc
    local t
    for _, v in pairs(M.adrtbl) do
        ow.reset(M.pin)
        ow.select(M.pin, v)
        ow.write(M.pin,0xBE,1)
        data = string.char(ow.read(M.pin))
        for i = 1, 8 do
            data = data .. string.char(ow.read(M.pin))
        end
        crc = ow.crc8(string.sub(data,1,8))
        if (crc == data:byte(9)) then
            t = (data:byte(1) + data:byte(2) * 256)
            if (t > 32767) then t = t - 65536 end
            t = t * 625 /10000
            local as = ""
            for ii = 1, #v do
                local hx = string.format("%02X", (string.byte(v, ii)))
                as = as ..hx
            end
            -- temptabl[as] = t
            temptabl[as] = string.format("%.2f",t)
            
        end
    end
    data = nil
    crc = nil
    t = nil
end

function M.getTemp(pin, del, ttable, call, unload)
    M.pin = M.pin or pin or 4
    if #M.adrtbl == 0 then
        M.addrs()
    else
        M.askT()
    end
    local getDS = tmr.create()
    getDS:register(del, tmr.ALARM_SINGLE, function (t) 
        t:unregister()
        getDS = nil
        M.readResult(ttable)
        if unload then
            M.getTemp = nil
            M.adrtbl = nil
            M.pin = nil
            M.setup = nil
            M.askT = nil
            M.readResult = nil
            unload()
            package.loaded["_ds18b20"]=nil
            call()
            return
        end

        if call then call() end
    end)
    getDS:start()
end
return M
