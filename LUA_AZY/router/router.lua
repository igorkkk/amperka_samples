function pubToBrocker(message, topic)
	topic = topic or ""
	topic = myClient.."/"..topic
	message = message or _G.message
	print("Sent "..message.." to "..topic)
	if publish then 
		m:publish(topic, message ,0,0) 
	end
end

function connecting()
    local starttmr = tmr.create()
	print('(Re)Connecting')
    local function getConnect()    
       if wifi.sta.status() == 5 then 
            print("Got WiFi!")
            pcall(m:connect(Broker, port, 0, 0,
                function(conn)
                    publish = true
					tmr.stop(starttmr)
					tmr.unregister(starttmr)
					starttmr = nil
                    m:subscribe(myClient.."/server",0, function(conn) print("Subscribe success") end)
                    pubToBrocker("ON", "state")
					--if not _G.pubmessage then
					--	_G.pubmessage = true
						pubToBrocker(_G.message, "reason")
                    -- end
					return true
            end)
            )
        end
    end

	starttmr:alarm(15000, 1, function()
        print("Time to Check!")
        getConnect()
    end)
    getConnect()
end

m:on("message", function(client, topic, data) 
  print(topic .. ":"..data ) 
  if data == 'start701' then
	pubToBrocker(wifi.sta.getip(), "ip")
	rtcmem.write32(0, 701)
	rtcmem.write32(2, 301)
	tmr.create():alarm(3000, 0, function() node.restart() end)
	end
  if data == "res205" then 
	pubToBrocker("Restart", "ip")
	rtcmem.write32(2, 300)
	tmr.create():alarm(3000, 0, function() node.restart() end)
  end	
end)

m:on("offline", function(con)
	publish = false
	_G.message = "Lost Broker"
	m:close()
	connecting()
end)

function check(call)
	checkTMR:stop()
	local servrs = {"www.google.com", "ya.ru", "www.yahoo.com", "www.mail.ru"}
    local servNo = 1
	
	local function isconnect(srv)
		net.dns.resolve(srv, function(sk, ip)
			if (ip == nil) or (ip == "10.0.0.1") then 
				print("DNS "..servrs[servNo].." fail!")
				if servNo < #servrs then
					servNo = servNo + 1
					isconnect(servrs[servNo])
				else
					checkTMR:start()
					if call then call(false) end
				end
			else 
				trying, restarting = 3, 0
				checkTMR:start()
				print("Got IP: "..ip.." at "..servrs[servNo] )
				pubToBrocker(node.heap(), "heap")
				if call then call(true) end
			end
		end)
	end
	isconnect(servrs[1])
end

function restartRouter()
	checkTMR:stop()
	local function relay(muv)
		if muv then
			gpio.write(routerPin,gpio.LOW)
			gpio.write(lightPin,gpio.HIGH)
		else
			gpio.write(routerPin,gpio.HIGH)
			gpio.write(lightPin,gpio.LOW)
		end
	end
	print("Restart Router!")
	relay(true)
	local stoprelay = tmr.create()
	stoprelay:alarm(15000, 0, function() 
        relay(false)
        print("Exit Restart Router!")
		tmr.unregister(stoprelay)
		stoprelay = nil
		countall()
		checkTMR:start()
	end)
end

function countall()
	restarting = restarting + 1
    print("Restart No "..restarting)
	if restarting == 1 then
        trying = 14
	elseif restarting == 2 then
        trying = 28
    else
        trying = 120
    end
end

connecting()

checkTMR = tmr.create()
checkTMR:register(checkperiod, 1, function()
    local ip = wifi.sta.getip()
    print("Trying: "..trying.." and IP:", ip)
    if (not ip and trying <= 2) then
        _G.message = "Lost WiFi"
		-- _G.pubmessage = false
        restartRouter()
    elseif trying <= 0  then
        _G.message = "Lost DNS"
		-- _G.pubmessage = false
        restartRouter()
    elseif ip then
        print("Check connection")
        check()
    end
	trying = trying - 1
	
end)
checkTMR:start()
