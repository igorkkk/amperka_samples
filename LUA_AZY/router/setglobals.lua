
_G.Broker = "iot.eclipse.org"
_G.port = 1883
_G.myClient = "connect022"
_G.pass = "superpassword"

_G.routerPin = 4
_G.lightPin = 3
gpio.mode(routerPin,gpio.OUTPUT)
gpio.mode(lightPin,gpio.OUTPUT)
gpio.write(lightPin,gpio.LOW)

_G.trying = 14
_G.restarting = 0
_G.checkperiod = 30000

_G.publish = false
_G.pubmessage = false

_G.m = mqtt.Client(myClient, 30, myClient, pass)
_G.m:lwt(myClient.."/state", "OFF", 0, 0)


local reason = rtcmem.read32(2)
if reason == 300 then 
	_G.message = "Manual Restart" 
elseif reason == 301 then
	_G.message = "After Srv"
else
	_G.message = "Lost Power"
end
print("Got reason:", _G.message)
dofile('router.lua')
--dofile('routerDS.lua')