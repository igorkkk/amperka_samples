function check(call)
	local servrs = {"www.google.com", "ya.ru", "www.yahoo.com", "www.mail.ru"}
    local servNo = 1
	
	local function isconnect(srv)
		net.dns.resolve(srv, function(sk, ip)
			if (ip == nil) or (ip == "10.0.0.1") then 
				print("DNS "..servrs[servNo].." fail!")
				if servNo < #servrs then
					servNo = servNo + 1
					isconnect(servrs[servNo])
				else
					if call then call(false) end
				end
			else 
				trying, restarting = 1, 0
				if publish then
					m:publish(myClient,reason,0,0,function(conn) 
					print("Sent reason", reason)
					reason = 10
					end)
				end
				print("Got IP: "..ip.." at "..servrs[servNo] )
				if call then call(true) end
			end
		end)
	end
	isconnect(servrs[1])
end
local function call(data)
	print('Got', data)
end

check(call) 
