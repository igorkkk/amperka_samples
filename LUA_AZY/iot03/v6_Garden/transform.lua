do
local temptable={}
file.remove("timeschd.lua")
local d,h,m,t
fr = file.open("sced.lua", "r")
if fr then
    repeat
        local line = (fr:readline())
        if line then
            d,h,m,t = string.match(line, "%s*(%d+)%s*,%s*(%d+):(%d+)%s*,%s*(%d+%.*%d*)")
            d = tonumber(d)
            h = tonumber(h)
            m = tonumber(m)
            t = tonumber(t)
            local timechck = d*24*60 + h*60 + m
            table.insert(temptable, {timechck, t})
        end
    until line == nil
end
fr.close()

table.sort(temptable, function(a,b)
    return b[1]>a[1]
end)
fw = file.open("timeschd.lua", "a+")
if fw then
    for _,v in pairs(temptable) do
        fw:writeline(""..v[1]..":"..v[2])
        print(""..v[1]..':'..v[2])
    end
end
fw:close()
end