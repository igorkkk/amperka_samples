--
do
	print('turnvalve works.')
	if dat.vlpowered then
		gpio.write(pins.open, gpio.HIGH)
		gpio.write(pins.close, gpio.HIGH)
	end
	if dat.powerpin then
		dat.powerpin:stop()
		dat.powerpin:unregister()
		dat.powerpin = nil
	end
	
	local function runpin(pin)
        dat.vlpowered = true
		gpio.write(pin, gpio.LOW)
		print('ON pin '..pin)
		dat.powerpin = tmr.create()
		dat.powerpin:register(30000, tmr.ALARM_SINGLE, function(tm) 
			gpio.write(pin, gpio.HIGH)
			dat.vlpowered = nil
			tm = nil
			dat.powerpin = nil
			print("OFF by 30 sec. pin "..pin)
		end)
		dat.powerpin:start()
	end
	
	if dat.startwater == 1 then
		runpin(pins.open)
		table.insert(topub, {'iswatering',"ON", 1})
		if not dat.tmrpoliv then 
			dat.tmrpoliv = tmr.create()
			dat.tmrpoliv:register(dat.poliv * 1000, tmr.ALARM_SINGLE, function(t) 
				table.insert(topub, {'iswatering',"OFF", 1})
				runpin(pins.close)
				t = nil
				dat.tmrpoliv = nl
			end)
			dat.tmrpoliv:start()
		end
	
	end
	
	if dat.startwater == 0 then
		print("Got stop watering!")
		if dat.tmrpoliv then
			dat.tmrpoliv:stop()
			dat.tmrpoliv:unregister()
			dat.tmrpoliv = nil
		end
		table.insert(topub, {'iswatering',"OFF", 1})
		runpin(pins.close)
	end
	dat.startwater = nil
end
