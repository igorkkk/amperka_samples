	-- Измеряем освещенность
	dat.lux = 1024 - adc.read(0)
	table.insert(topub, {'lux',dat.lux})
	print("lux: "..dat.lux)
	-- Узнаем который час
	local tm = rtctime.epoch2cal(rtctime.get()+3*60*60)
	--print(string.format("%02d:%02d:%02d", tm.hour, tm.min, tm.sec))
	-- Если с часами все нормально и знаем время заката и рассвета
	if tm.year ~= 1970 and dat.setH ~= 100 and dat.riseH ~= 100 then
		-- Время минут сейчас
		local tmnow = tm.hour * 60 + tm.min
		-- Время минут заката
		local nght = dat.setH * 60 + dat.setM
		-- Время  минут рассвета
		local day = dat.riseH * 60 + dat.riseM
	
		-- Сейчас день или ночь по времени?
		if tmnow > nght and dat.now == 'Day' then
			dat.now = 'Night'
		elseif tmnow > day and dat.now == 'Night' then
			dat.now = 'Day'
		end
	else
		-- Плохо со временем -  узнаем по освещенности
		if dat.lux > 100 and dat.now == 'Night' then
			dat.now = 'Day'
		elseif  dat.lux < 5  and dat.now == 'Day' then
			dat.now = 'Night'
		end
		
	end
	print('Now is '..dat.now)
	
	-- Если это включение устройства (устанавливается при старте)
	if dat.powerON  then
		dat.powerON = nil
		-- выключаем полив а всякий случай
		dat.startwater = 0
		-- Типа решение о поливе уже принято
		if dat.now == 'Day' then 
			dat.makedesDay = 1
		else
			dat.makedesNgt = 1
		end
	end
	-- Сейчас день и мы не принимали решени о поливе
if dat.now == 'Day' and dat.makedesDay == 0 then
	-- День наступил - сбрасываем флаг принятия ночных решений
	dat.makedesNgt = 0
	-- Соотносим с количеством дней интервала между поливами
	-- Если выждали нормально
	if dat.lastDay >= dat.intervalDay then
		-- Поливаем
		dat.lastDay = 0
		print('Water at Day!')
		dat.startwater = 1
	else
		-- Иначе не поливаем но добавляем день
		dat.lastDay = dat.lastDay + 1
		print('Day Interval:', dat.lastDay)
		dofile('savedata.lua')
	end
	-- Оповещаем о решении брокер
	table.insert(topub, {'lastDay', dat.lastDay})
	-- Выставляем флаг что все решено
	dat.makedesDay = 1
end 

-- Ночью действуем аналогично дневному алгоритму

if dat.now == 'Night' and dat.makedesNgt == 0 then
	dat.makedesDay = 0
	if dat.lastNgt >= dat.intervalNgt then
		dat.lastNgt = 0
		print('Water at Night!')
		dat.startwater = 1
	else
		dat.lastNgt = dat.lastNgt + 1
		print('Night interval:', dat.lastNgt)
		dofile('savedata.lua')			
	end
	table.insert(topub, {'lastNgt', dat.lastNgt})
	dat.makedesNgt = 1
end 
-- Если есть хоть какое-то значение dat.startwater:
if dat.startwater then
	dofile('turnvalve.lua')
end

tm, tmnow, nght, day = nil, nil, nil, nil