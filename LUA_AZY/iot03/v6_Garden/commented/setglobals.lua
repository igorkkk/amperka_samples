myClient = "test001"  -- (!!!) Заменить на свое!
dat = {
	target = 22, -- (!!!) Целевая температура отопителя по умолчанию
	askds = '', -- (!!!) DS18b20 адрес для управления отопителем
	-- askds = 't0054', -- Пример установки адреса
	poliv = 3600, -- (!!!) Время полива в секундах
-----------------------------------------
	dispatch = false,
	light = 'OFF',
	lightF = false,
	arm = 'OFF',
	heat = 'OFF',
	broker = false,
	siren = 'OFF',
	auto = 'ON',
	clpub = 0, -- count lost publications
	----- Восход/закат  
	-- 100 - до получения правильных данных
	setH = 100,  
	setM = 100,
	riseH = 100,
	riseM = 100,
	setHMcount = 10,
	------ Полив
	now = 'Night',
	lux = 0,
	makedesDay = 0,
	lastDay = 0,
	intervalDay = 0,
	makedesNgt = 0,
	lastNgt = 0,
	intervalNgt = 0,
	-- Включение после подачи питания
	-- чтобы сразу не начался полив
	powerON = 'true'
}
-- Если вам лень переименовывать клиент - я переименую
if myClient == "test001" then 
	myClient = "home_"..node..chipid()
	print('\n\n\nClient is: '..myClient..'!!!!!!!!!\n\n\n')
end
pins = { --Ноги
	light = 0, -- освещение
	heat = 7, -- нагреватель
	siren = 6, -- сирена
	open = 8, -- открыть кран
	close = 9, -- закрыть кран
	pir = 5, -- ПИР датчик
}
for _,v in pairs(pins) do
	gpio.mode(v, gpio.OUTPUT)
end
gpio.mode(pins.pir,gpio.INPUT)

topub = {}
killtop = {}

-- Перерабатываем расписание в машинное 
if file.exists('transform.lua') and file.exists('sced.lua') and  not file.exists('timeschd.lua') then
      dofile('transform.lua')
end

-- Восстанавливаем сохраненные данные с прошлой сессии
if file.exists('setsaveddat.lua') then
      dofile('setsaveddat.lua')
end

-- Если сохранен режим охраны - ставим 
if dat.arm == "ON" then
	dofile('setarm.lua')
end
-- Коннект к брокеру
dofile('setmqtt.lua')
-- Главная программа
dofile('main.lua')