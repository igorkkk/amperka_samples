-- Функция - вкл/выкл
local function setlight(st)
	dat.light = 'ON'
	local wr = st == 'ON' and 1 or 0
	gpio.write(pins.light, wr)
	print("Light set "..st)
end

-- Если устройство в режиме охраны и свет включен - выключаем
if dat.arm == 'ON'  then
	if  dat.light == 'ON' then setlight('OFF')  end
-- Иначе, если время получено правильно - действуем
elseif dat.setH ~= 100 and dat.riseH ~= 100 then
	-- Получим текущее время
	local tm = rtctime.epoch2cal(rtctime.get()+3*60*60)
	--print(string.format("%02d:%02d:%02d", tm.hour, tm.min, tm.sec))
	-- Если время было получено с сервера
	if tm.year ~= 1970 then
		-- Минут с начала дня
		local tmnow = tm.hour * 60 + tm.min
		-- Минут с начала дня для включения
		local swon = dat.setH * 60 + dat.setM + 30
		-- Минут с начала дня для выключения по пьянке
		local swoff = dat.riseH * 60 + dat.riseM - 30
		--Минут с начала дня для ботаников - 23:30 
		local swoffsleep = 23*60+30
		
		-- Включаем освещение
		if tmnow > swon and dat.light == 'OFF' then 
			setlight('ON')
		end
		
		-- Пора спать
		if (not dat.lightF) and (tmnow > swoffsleep) and  dat.light == 'ON' then
			setlight('OFF')
		end
		
		-- Включил и забыл свет выключить - как не стыдно!
		if math.abs(tmnow - swoff) < 2 and dat.light == 'ON' then
			dat.lightF = false
			setlight('OFF')
		end

	end
end

tm, tmnow, swon, swoff, swoffsleep, setlight, wr  = nil, nil, nil, nil, nil, nil, nil