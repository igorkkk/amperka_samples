do
	dat.publ = true
	table.insert(topub, {'heap', node.heap()})
	local tp
	local strtpu = #topub
	local function punow()
		if #topub ~= 0 and wifi.sta.getip() and dat.broker then
			-- print('pub: ', #topub)
			tp = table.remove(topub)
			tp[2] = tp[2] or ""
			tp[3] = tp[2] == "" and "1" or "0"
			m:publish(myClient.."/"..tp[1], tp[2], 2, tp[3], punow)
		else
			tp = nil
			dat.publ = false
			punow = nil
			-- Сбрасываем счетчик провало публикации
			dat.clpub = 0
			dat.dispatch = false
			print('Published!')
			return
		end
	end
	punow()
	-- Костыль
	---[[
	-- Создаем таймер
	tmr.create():alarm(15000, 0, function(t)
		t = nil
		-- Если через 15 секунд (от начала) публикации не сброшен флаг
		if dat.publ == true then
			-- Извещаем о кличестве неотправленны топиков
			print('----------------------- Kill Publishing! lost: ', #topub)
			-- Если неотправлено много - считаем количество провалов 
			if strtpu - #topub < 3 then
				dat.clpub = dat.clpub + 1
			end
			-- Возвращаем флаги и таблицыв правильное состояние
			dat.dispatch = false
			topub = {}
			tp = nil
			dat.publ = false
			punow = nil
			-- Три провала подряд - пересоединение 
			if dat.clpub > 3 then
				print('Restart MQTT!')
				dat.clpub = 0
				m = nil
				dofile 'setmqtt.lua'
			end
		end
	end)
	--]]
end