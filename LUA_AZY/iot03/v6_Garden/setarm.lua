if dat.arm == "ON" then
	dat.light = 'OFF'
	gpio.write(pins.light, gpio.LOW)
	print("Light set OFF")
	function siren()
		gpio.trig(pins.pir)
		print('Pir Disarmed')
		dofile('sirennow.lc')
	end
	gpio.trig(pins.pir, "up", siren)
	print('Pir Armed')
elseif dat.arm == "OFF" then
	gpio.trig(pins.pir)
	print('Pir Disarmed')
	gpio.write(pins.siren, gpio.LOW)
	print('Siren stop')
	siren = nil
	if dat.tmrsiren then
		dat.tmrsiren:stop()
		dat.tmrsiren:unregister()
		dat.tmrsiren = nil
	end
end