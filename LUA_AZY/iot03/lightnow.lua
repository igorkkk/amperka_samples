-- dat.lux = 1024 - adc.read(0)
print('lux = '..dat.lux)
if dat.arm == 'ON' then
	print('Armed, lux:', dat.lux)
	return
end

function light()
	tm = rtctime.epoch2cal(rtctime.get()+3*60*60)
	print(string.format("%04d/%02d/%02d %02d:%02d:%02d", tm.year, tm.mon, tm.day, tm.hour, tm.min, tm.sec))
	
	if (tm.hour > 18) and (dat.light == 'OFF') and (dat.lux < 5) then
		dat.light = 'ON'
		gpio.write(pins.light, gpio.HIGH)
		print("Lux: Light is ON")
	end
	
	if dat.light == 'ON' and tm.hour == 23 and tm.min > 30 then
		dat.light = 'OFF'
		gpio.write(pins.light, gpio.LOW)
		print("Time to Light: OFF")
	end
end

function checktime()
	sntp.sync("ntp1.stratum1.ru",
	  function()
		print('Got NTP!')
		dat.countntp = 0
		light()
	  end,
	  function()
	   print('NTP failed!')
	   light()
	  end)
end
dat.countntp = dat.countntp + 1
print('countntp = '..dat.countntp)
if dat.countntp > 5 then 
	checktime()
else
	light()
end
