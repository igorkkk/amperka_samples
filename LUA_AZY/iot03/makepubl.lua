if dat.broker then
	local list = {
		'target',
		'light',
		'arm',
		'heat',
		'lux',
		'siren'
	}
	for _, v in pairs(list) do
		table.insert(topub, {v, dat[v]})	
	end
	list = nil
	if not dat.publ then dofile('pubnow.lua') end
else
	topub = {}
end
