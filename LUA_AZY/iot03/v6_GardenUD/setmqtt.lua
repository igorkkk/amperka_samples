if not m then
    m = mqtt.Client( myClient, 120, myClient, 'passfor'..myClient)
    m:lwt(myClient..'/state', "OFF", 0, 1)
    m:on("message", function(conn, topic, dt)
        local top = string.gsub(topic, myClient.."/command/","")
        print('Got now:',top, ":", dt)
        if dt then
            table.insert(killtop, {top, dt})
            if not dat.analiz then
                dofile("analize.lc")
            end
        end
    end)
    m:on("offline", function(con)
        dat.broker = false
        dofile('setmqtt.lc')
    end)
end

m:close()

local count = 0
local connecting = function(getmq)

    if wifi.sta.status() == 5 then
        print('Got wifi')
        tmr.stop(getmq)
        tmr.unregister(getmq)
        getmq = nil
		dofile('getsun.lc')
		sntp.sync({"ntp1.stratum1.ru", "ntp4.stratum2.ru", "ntp3.stratum2.ru"},
			function()
				print('Got NTP!')
			end,
			function()
			   print('NTP failed!')
			end, 
		true)
		print('\n\n'..broker..'!!!!!!!\n\n')
        m:connect(broker, 1883, 0, 0,
        function(con)
            print("Connected to Broker")
            m:subscribe(myClient.."/command/#",0, function(conn)
                print("Subscribed.")
            end)
            m:publish(myClient..'/state',"ON",0,1)
            dat.broker = true
            count = nil
        end,
        function(con)
            dofile('setmqtt.lc')
        end)
    else
        print("Wating for WiFi "..count.." times")
        count = count + 1
    end
end

tmr.create():alarm(5000, 1, function(t)
    connecting(t)
end)