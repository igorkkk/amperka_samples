table.foreach(dat, print)
dispatch = function()
	print('\n')
	dat.dispatch = true
	local threads = {}
	local filestorun = {
		'getsun.lc',
		'lightnow.lc',
		'scedset.lc',
		'temper.lc',
		'garden.lc',
		'makepubl.lc'
	}
	local co

	for _, v in ipairs(filestorun) do
		co = coroutine.create(function ()
			return dofile(v)
		end)
		table.insert(threads, co)
	end
	
	local nextc = 1
	local function ne()
		if nextc <= #threads then
			print('no '..nextc, filestorun[nextc] , (coroutine.resume(threads[nextc])))
			nextc = nextc + 1
		end
	end
	ne()
	tmr.create():alarm(3000, 1, function(t)
		if nextc <= #threads then 
			ne()
		else
			t:stop()
			t:unregister()
			t = nil
			nextc, ne, threads = nil, nil, nil
		end
	end)

end

tmr.create():alarm(60000, 1, dispatch)