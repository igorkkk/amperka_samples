myClient = "test001"
broker = 'iot.eclipse.org'
dat = {
	dispatch = false,
	target = 22,
	askds = 't0054', -- DS18b20 address
	--askds = '', -- DS18b20 address
	poliv = 3600,
	
	light = 'OFF',
	lightF = false,
	arm = 'OFF',
	heat = 'OFF',
	broker = false,
	siren = 'OFF',
	auto = 'ON',
	clpub = 0, -- count lost publications
	setH = 100,
	setM = 100,
	riseH = 100,
	riseM = 100,
	setHMcount = 10,
	------ garden
	now = 'Night',
	lux = 0,
	makedesDay = 0,
	lastDay = 0,
	intervalDay = 0,
	makedesNgt = 0,
	lastNgt = 0,
	intervalNgt = 0,
	powerON = 'true'
}

pins = {
	light = 0,
	heat = 7,
	siren = 6,
	open = 8,
	close = 9,
	pir = 5,
}
for _,v in pairs(pins) do
	gpio.mode(v, gpio.OUTPUT)
end
gpio.mode(pins.pir,gpio.INPUT)

--[[
if myClient == "test001" then 
	myClient = "home_"..node.chipid()
	print('\n\n\nClient is: '..myClient..'    !!!!!!!!!\n\n\n')
end
--]]
topub = {}
killtop = {}

if file.exists('transform.lc') and file.exists('sced.lua') and  not file.exists('timeschd.lua') then
      dofile('transform.lc')
end

if file.exists('setsaveddat.lua') then
      dofile('setsaveddat.lua')
end
if dat.arm == "ON" then
	dofile('setarm.lua')
end

dofile('setmqtt.lc')
dofile('main.lc')