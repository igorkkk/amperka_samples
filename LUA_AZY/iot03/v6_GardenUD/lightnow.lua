local function setlight(st)
	dat.light = st
	local wr = (st == 'ON') and 1 or 0
	gpio.write(pins.light, wr)
	print("Light set "..st)
end

if dat.arm == 'ON'  then
	if  dat.light == 'ON' then setlight('OFF')  end
elseif dat.setH ~= 100 and dat.riseH ~= 100 then
	local tm = rtctime.epoch2cal(rtctime.get()+3*60*60)
	--print(string.format("%02d:%02d:%02d", tm.hour, tm.min, tm.sec))
	if tm.year ~= 1970 then
		local tmnow = tm.hour * 60 + tm.min
		local swon = dat.setH * 60 + dat.setM + 30
		local swoff = dat.riseH * 60 + dat.riseM - 30
		local swoffsleep = 23*60+30
		
		if tmnow > swon and dat.light == 'OFF' then 
			setlight('ON')
		end
	
		if (not dat.lightF) and (tmnow > swoffsleep) and  dat.light == 'ON' then
			setlight('OFF')
		end
		
		if math.abs(tmnow - swoff) < 2 and dat.light == 'ON' then
			dat.lightF = false
			setlight('OFF')
		end

	end
end

tm, tmnow, swon, swoff, swoffsleep, setlight, wr  = nil, nil, nil, nil, nil, nil, nil