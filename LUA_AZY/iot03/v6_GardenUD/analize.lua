dat.analiz = true
local target
if #killtop ~= 0 then
	local com = table.remove(killtop)
	local top = com[1]
	local dt = com[2]
	if top and dt then
		if top == "light" then
			if dt == "ON" then
				dat.light = "ON"
				dat.lightF = true
				gpio.write(pins.light, gpio.HIGH)
				print("Light is ON")
			end
			if dt == "OFF" then
				dat.light = "OFF"
				dat.lightF = false
				gpio.write(pins.light, gpio.LOW)
				print("Light is OFF")
			end
		elseif top == "water" then
			if dt == "ON" then
				dat.startwater = 1
				print("Watering Now")
			end
			if dt == "OFF" then
				dat.startwater = 0
				print("Stop Water Now")
			end
		elseif top == "day" then 
			dat.intervalDay = tonumber(dt) or 0
			dat.lastDay = 0
			print('Set dat.intervalDay at '..dat.intervalDay)
			dofile('savedata.lc')
			table.insert(topub, {'intervalDay', dat.intervalDay})
		elseif top == "night" then  
			dat.intervalNgt =  tonumber(dt) or 0
			dat.lastNgt = 0
			print('Set dat.intervalNgt at '..dat.intervalNgt)   
			dofile('savedata.lc')
			table.insert(topub, {'intervalNgt', dat.intervalNgt})
		elseif top == "target" then 
			target = tonumber(dt) or 75
			if target == 75 then
				print('Got Wrong Target!')
			else
				if target < 15 then 
					dat.target = 15
				elseif target > 27 then 
					dat.target = 27
				else
					dat.target = target
				end
				dat.auto = 'OFF'
			end
			print('Set Target Temp At '..dat.target)
			dofile("savedata.lc")
		elseif	top == "arm" then
			if dt == "ON" then
				dat.arm = "ON"
				print("Armed")
			end
			if dt == "OFF" then
				dat.arm = "OFF"
				print("Disarmed")
			end
			dofile('setarm.lc')
			dofile("savedata.lc")
		elseif	top == "auto" then
			if dt == "ON" then
				dat.auto = "ON"
				print("Auto sets ON")
			end
			if dt == "OFF" then
				dat.auto = "OFF"
				print("Auto sets ON")
			end
		end
	end	
	com, top, dt, target = nil, nil, nil, nil
	dofile('analize.lc')
else
	dat.analiz = nil
	com, top, dt, target = nil, nil, nil, nil
	if not dat.dispatch then dofile('makepubl.lc') end
end