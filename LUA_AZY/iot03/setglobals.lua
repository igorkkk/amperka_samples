myClient = "test001"
pins = {
	light = 0,
	heat = 7,
	siren = 6,
	pir = 5
}
for _,v in pairs(pins) do
	gpio.mode(v, gpio.OUTPUT)
end
gpio.mode(pins.pir,gpio.INPUT)

dat = {
	target = 22,
	light = 'OFF',
	arm = 'OFF',
	heat = 'OFF',
	broker = false,
	siren = 'OFF',
	countntp = 100,
	lux = 0
}

topub = {}
killtop = {}

if file.exists('setsaveddat.lua') then
      dofile('setsaveddat.lua')
end
if dat.arm == "ON" then
	dofile('setarm.lua')
end

---- Set MQTT ----
--[[
m = mqtt.Client( myClient, 60, myClient, 'passfor'..myClient)
m:lwt(myClient..'/state', "OFF", 0, 1)
m:on("message", function(conn, topic, dt)
	local top = string.gsub(topic, myClient.."/command/","")
	print('Got now:',top, ":", dt)
	if dt then
		table.insert(killtop, {top, dt})
		if not dat.analiz then
			dofile("analize.lua")
		end
	end
end)
m:on("offline", function(con)
	dat.broker = false
	dofile('setmqtt.lua')
end)
--]]
------------------

dofile('setmqtt.lua')
dofile('main.lua')