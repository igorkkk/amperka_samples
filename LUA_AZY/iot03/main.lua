print('main')
table.foreach(dat, print)

dispatch = function()
	local threads = {}
	local filestorun = {
		'lightnow.lua',
		'scedset.lua',
		'temper.lua',
		'makepubl.lua'
	}
	for _, v in ipairs(filestorun) do
		local co = coroutine.create(function ()
			dofile(v)
		end)
		table.insert(threads, co)
	end
	
	for k , v in ipairs(threads) do
		local status = coroutine.resume(v)
		print('no '..k ,status)
	end
	--[[	
	local status = coroutine.resume(threads[1])
	print('first',status)
	status = coroutine.resume(threads[2])
	print('second',status)
	print(node.heap())
	--]]
	thread, filestorun, status = nil, nil, nil
end

tmr.create():alarm(60000, 1, dispatch)