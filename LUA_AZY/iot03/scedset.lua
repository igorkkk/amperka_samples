if not dat then dat = {} end
tm = rtctime.epoch2cal(rtctime.get()+3*60*60)
print(string.format("%04d/%02d/%02d %02d:%02d:%02d", tm.year, tm.mon, tm.day, tm.hour, tm.min, tm.sec))

if tm.year == '1970' then 
	tm = nil
	print('Bad Time!!!!!!!!!!!!!!!')
	return 
end

tm.wday = (tm.wday - 1) == 0 and 7 or (tm.wday - 1)
local timenow = tm.wday*24*60+tm.hour*60+tm.min
tm = nil

local d,t,toset, line
local gotrecord = false

if file.open("timeschd.lua", "r") then
    local function  settg(t)
        if dat.target ~= t then
            dat.target = t
            print('Set temp '..dat.target)
        else
            print('Temp is eq! Not changed '..dat.target)
        end
    d,t,toset, gotrecord, line, timenow, analize, settg = nil, nil, nil, nil, nil, nil, nil, nil
    end
    
    function analize(call)
        repeat
            line = (file.readline())
            if line then
                d,t = string.match(line, "(%d+):(%d+%.*%d*)")
                d = tonumber(d)
                t = tonumber(t)
                if timenow >= d  then
                    gotrecord = true
                    toset = t
                end
            end
        until line == nil
        file.close()
        if not gotrecord then
            toset = t
        end
        if call then call(toset) end
    
    end
    analize(settg)
else
    d,t,toset, gotrecord, line, timenow, analize, settg = nil, nil, nil, nil, nil, nil, nil, nil
end