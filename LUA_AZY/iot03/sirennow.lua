if dat.arm == "ON" then
	print('Siren start')
	table.insert(topub, {'siren', 'ON'})
	dat.siren = 'ON'
	if not dat.publ then dofile('pubnow.lua') end
	dat.tmrsiren = tmr.create()
	gpio.write(pins.siren, gpio.HIGH)
	dat.tmrsiren:alarm(30000, 0, function(t)
		t = nil
		dat.tmrsiren = nil
		gpio.write(pins.siren, gpio.LOW)
		print('Siren stop')
		gpio.trig(pins.pir, "up", siren)
		print('Pir Armed')
		table.insert(topub, {'siren', 'OFF'})
		dat.siren = 'OFF'
		if not dat.publ then dofile('pubnow.lua') end
	end)
end
