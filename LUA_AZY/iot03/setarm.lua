if dat.arm == "ON" then
	dat.light = 'OFF'
	gpio.write(pins.light, gpio.LOW)
	function siren()
		print('Got Pin Alarm!!!')
		gpio.trig(pins.pir)
		print('Pir Disarmed')
		dofile('sirennow.lua')
	end
	gpio.trig(pins.pir, "up", siren)
	print('Pir Armed')
elseif dat.arm == "OFF" then
	gpio.trig(pins.pir)
	print('Pir Disrmed')
	gpio.write(pins.siren, gpio.LOW)
	print('Siren stop')
	siren = nil
	if dat.tmrsiren then
		dat.tmrsiren:stop()
		dat.tmrsiren:unregister()
		dat.tmrsiren = nil
	end
end