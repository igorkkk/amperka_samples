myClient = "test001"
pins = {
	light = 0,
	heat = 7,
	siren = 6,
	pir = 5
}
for _,v in pairs(pins) do
	gpio.mode(v, gpio.OUTPUT)
end
gpio.mode(pins.pir,gpio.INPUT)

dat = {
	dispatch = false,
	target = 22,
	askds = 't0054', -- DS18b20 address
	--askds = '', -- DS18b20 address
	light = 'OFF',
	lightF = false,
	arm = 'OFF',
	heat = 'OFF',
	broker = false,
	siren = 'OFF',
	auto = 'ON',
	clpub = 0, -- count lost publications
	setH = 100,
	setM = 100,
	riseH = 100,
	riseM = 100,
	setHMcount = 10
}

topub = {}
killtop = {}

if file.exists('transform.lua') and file.exists('sced.lua') and  not file.exists('timeschd.lua') then
      dofile('transform.lua')
end

if file.exists('setsaveddat.lua') then
      dofile('setsaveddat.lua')
end
if dat.arm == "ON" then
	dofile('setarm.lua')
end

dofile('setmqtt.lua')
dofile('main.lua')