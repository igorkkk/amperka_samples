local tm = rtctime.epoch2cal(rtctime.get()+3*60*60)
-- print(string.format("%02d:%02d:%02d", tm.hour, tm.min, tm.sec))

tm.wday = (tm.wday - 1) == 0 and 7 or (tm.wday - 1)
local timenow = tm.wday*24*60+tm.hour*60+tm.min
local d,t,toset, line, settg, analize, clear
local gotrecord = false

clear = function()
  tm, d,t,toset, gotrecord, line, timenow, analize, settg, clear = nil, nil, nil, nil, nil, nil, nil, nil, nil, nil
end

if tm.year ~= 1970 and dat.auto == 'ON' and file.open("timeschd.lua", "r")  then
    settg = function(t)
        if dat.target ~= t then
            dat.target = t
            print('Set temp: '..dat.target)
        else
            print('Remain temp: '..dat.target)
        end
		clear()
    end
    
    analize = function(call)
        repeat
            line = (file.readline())
            if line then
                d,t = string.match(line, "(%d+):(%d+%.*%d*)")
                d = tonumber(d)
                t = tonumber(t)
                if timenow >= d  then
                    gotrecord = true
                    toset = t
                end
            end
        until line == nil
        file.close()
        if not gotrecord then
            toset = t
        end
        if call then call(toset) end
    
    end
    analize(settg)
else
    clear()
end