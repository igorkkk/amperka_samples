do
local offset = 3 
-- 'api.sunrise-sunset.org/json?lat=55.75583&lng=37.61778'
if dat.setHMcount < 10 then
	dat.setHMcount = dat.setHMcount + 1
	print('Just:', dat.setH, dat.setM, dat.riseH, dat.riseM)
else	
	local setH, setM, riseH, riseM, makeDig
	local request = "GET /json?lat=55.75583&lng=37.61778&formatted=0 HTTP/1.1\r\n"..	 
		"Host: api.sunrise-sunset.org\r\n\r\n"
	conn=net.createConnection(net.TCP, 0) 
	conn:on("connection", function(conn, payload) conn:send(request) end)
	conn:on("receive", function(conn, payload)
		local ttmm = string.sub(payload,string.find(payload,'sunrise')+20,string.find(payload,'sunset')+24)
		payload = nil
		--print(ttmm)
		riseH, riseM, setH, setM = string.match(ttmm,'(%d+):(%d+).+sunset.+T(%d+):(%d+)')
		--print(setH, setM, riseH, riseM)
		
		makeDig = function(dig, trans)
			dig = tonumber(dig)
			if trans then
				dig = (dig + offset < 24) and (dig + offset) or (dig - 21)
			end
			return dig
		end
		if setH and setM then
			dat.setH = makeDig(setH, true)
			dat.setM = makeDig(setM)
		end
		if riseH and riseM then
			dat.riseH = makeDig(riseH, true)
			dat.riseM = makeDig(riseM)
		end

		dat.setHMcount =  0
		print('Set:', dat.setH, dat.setM, dat.riseH, dat.riseM)
		setH, setM, riseH, riseM, request, makeDig, ttmm, offset = nil, nil, nil, nil, nil, nil, nil, nil
		conn:close()
		end) 
	conn:connect(80, 'api.sunrise-sunset.org')
end
end