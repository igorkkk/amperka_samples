if dat.arm == "ON" then
	print('Siren start')
	-- Сразу готовим информацию о нарушении на публикацию
	table.insert(topub, {'siren', 'ON'})
	dat.siren = 'ON'
	-- Запускаем публткацию
	if not dat.publ then dofile('pubnow.lua') end
	-- В глобальной таблице создаем таймер выключения
	dat.tmrsiren = tmr.create()
	-- Начинаем орать сиреной
	gpio.write(pins.siren, gpio.HIGH)
	-- Запускаем таймер выключения сирены через 30 сек.
	dat.tmrsiren:alarm(30000, 0, function(t)
		t = nil
		dat.tmrsiren = nil
		gpio.write(pins.siren, gpio.LOW)
		print('Siren stop')
		-- Возвращаем наблюдение за ногой ПИР
		gpio.trig(pins.pir, "up", siren)
		print('Pir Armed')
		-- Оповещаем брокер что сирена заглохла
		table.insert(topub, {'siren', 'OFF'})
		dat.siren = 'OFF'
		if not dat.publ then dofile('pubnow.lua') end
	end)
end
