dat.analiz = true
local target
if #killtop ~= 0 then
	local com = table.remove(killtop)
	local top = com[1]
	local dt = com[2]
	if top and dt then
		if top == "light" then
			if dt == "ON" then
				dat.light = "ON"
				dat.lightF = true
				gpio.write(pins.light, gpio.HIGH)
				print("Light is ON")
			end
			if dt == "OFF" then
				dat.light = "OFF"
				dat.lightF = false
				gpio.write(pins.light, gpio.LOW)
				print("Light is OFF")
			end
		elseif top == "target" then 
			target = tonumber(dt) or 75
			if target == 75 then
				print('Got Wrong Target!')
			else
				if target < 15 then 
					dat.target = 15
				elseif target > 27 then 
					dat.target = 27
				else
					dat.target = target
				end
				dat.auto = 'OFF'
			end
			print('Set Target Temp At '..dat.target)
			-- dofile("savedata.lua")
		elseif	top == "arm" then
			if dt == "ON" then
				dat.arm = "ON"
				print("Armed")
			end
			if dt == "OFF" then
				dat.arm = "OFF"
				print("Disarmed")
			end
			dofile('setarm.lua')
			-- dofile("savedata.lua")
		elseif	top == "auto" then
			if dt == "ON" then
				dat.auto = "ON"
				print("Auto sets ON")
			end
			if dt == "OFF" then
				dat.auto = "OFF"
				print("Auto sets ON")
			end
		end
	end	
	com, top, dt, target = nil, nil, nil, nil
	dofile('analize.lua')
else
	dat.analiz = nil
	com, top, dt, target = nil, nil, nil, nil
	if not dat.dispatch then dofile('makepubl.lua') end
end