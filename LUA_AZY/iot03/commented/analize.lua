dat.analiz = true -- Анализ данных с брокера начался
if #killtop ~= 0 then -- В таблице для анализа есть данные
	-- Выдергиваем запись из таблицы
	local com = table.remove(killtop) 
	local top = com[1]
	local dt = com[2]
	-- Разбираем пару топик-дата
	if top and dt then 
		-- Здесь тема освещения
		if top == "light" then
			if dt == "ON" then
				dat.light = "ON"
				gpio.write(pinlight, gpio.HIGH)
				print("Light is ON")
			end
			if dt == "OFF" then
				dat.light = "OFF"
				gpio.write(pinlight, gpio.LOW)
				print("Light is OFF")
			end
		-- Тема установки целевой температуры	
		elseif top == "target" then 
			-- Формируем цифру, а если мусор - 75
			local target = tonumber(dt) or 75
			-- Дальше защищаемся от неправильной уставки
			if target == 75 then
				print('Got Wrong Target!')
			else
				if target < 15 then 
					dat.target = 15
				elseif target > 27 then 
					dat.target = 27
				else
					dat.target = target
				end
			end
			target = nil
			print('Set Target Temperature At '..dat.target)
			dofile('savedata.lua')
		end
	end	
	com, top, dt = nil, nil, nil
	-- Вызываем себя рекурсивно для проверки данных от брокера
	dofile('analize.lua')
else
	-- Данные для анализа кончились - уничтожаем флаг
	dat.analiz = nil
end