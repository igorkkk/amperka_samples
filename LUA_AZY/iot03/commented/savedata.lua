do
	-- "lst" - таблица с перечнем ключей, которые будут
	-- сохранены из рабочей таблицы 'dat'
	local lst = dofile('listtosave.lua')
	-- создаем и открываем временный файл
	tmp = file.open("tmp.lua", "w")
	if tmp then
		local line
		-- Выбираем значения из перечня полей для сохранения 
		for _, v in pairs(lst) do
			-- Формируем строку для сохранения, печатаем и сохраняем
			-- во временный файл
			line = "dat."..v[1].."="..dat[v[1]].."\n"
			print('write: '..line)
			tmp:write(line)     
		end
	end
	tmp:close(); tmp = nil
	-- Удаляем старый файл и заменяем новым
	file.remove("setsaveddat.lua")
	file.rename("tmp.lua", "setsaveddat.lua")
	lst = nil
end
