do
dat.publ = true -- Флаг публикации
--[[
-- Это для преобразования таблиц с ключом в индексируемые
-- потребуется позже
for k,v in pairs(tohome) do
	table.insert(topub, {k, v})
end
--]]
-- Всегда публикуем 'heap', за ним надо наблюдать
table.insert(topub, {'heap', node.heap()})
local function punow()
	-- Если в таблице для публикации что-то есть и она возможна
	if #topub ~= 0 and wifi.sta.getip() and dat.broker then
		-- Изымаем элемент из таблицы
		local tp = table.remove(topub)
		-- Элемент с индексом 2 (сообщение топика) может отсутствовать
		tp[2] = tp[2] or ""
		-- Если сообщения нет, то поднимаем флаг "Retained" 
		-- чтобы удалить принятое сообщение
		tp[3] = tp[2] == "" and "1" or "0"
		-- Публикуем и вызываем себя рекурсивно:
		m:publish(myClient.."/"..tp[1], tp[2], 0, tp[3], punow)
	else
		tp = nil
		dat.publ = nil
		punow = nil
		print('Published!')
		return
	end
end
punow()
end