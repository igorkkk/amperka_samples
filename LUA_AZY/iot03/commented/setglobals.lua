-- Имя устройства
myClient = "test001"
-- Нога для управления освещением
pinlight = 0
gpio.mode(pinlight, gpio.OUTPUT)
-- Рабочая таблица
dat = {}
dat.target = 22 -- Целевая температура
dat.light = 'OFF' -- Освещение выключено
dat.broker = false -- Связи с брокером нет
-- Если файл 'setsaveddat.lua' есть - выполним его
if file.exists('setsaveddat.lua') then
      dofile('setsaveddat.lua')
end

-- Таблица для публикации на брокер
topub = {}
-- Таблица для анализа информации от брокера
killtop = {}
-- Устанавливаем связь с брокером и выполняем главную программу
dofile('setmqtt.lua')
dofile('main.lua')