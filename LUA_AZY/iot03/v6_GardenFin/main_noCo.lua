table.foreach(dat, print)
dispatch = function()
	threads = {
		function() dofile('getsun.lua') end,
		function() dofile('lightnow.lua')end,
		--function() dofile('scedset.lua')end,
		function() dofile('temper.lua')end,
		function() dofile('garden.lua')end,
		function() dofile('makepubl.lua')end
	}
	local nextc = 1
	local function ne()
		if nextc <= #threads then
			print('no '..nextc)
			threads[nextc]()
			
			nextc = nextc + 1
		end
	end
	ne()
	tmr.create():alarm(3000, 1, function(t)
		if nextc <= #threads then 
			ne()
		else
			t:stop()
			t:unregister()
			t = nil
			nextc, ne, threads = nil, nil, nil
		end
	end)
end
tmr.create():alarm(60000, 1, dispatch)