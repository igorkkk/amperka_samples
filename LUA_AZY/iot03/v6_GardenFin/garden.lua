	dat.lux = 1024 - adc.read(0)
	table.insert(topub, {'lux',dat.lux})
	print("lux: "..dat.lux)
	local tm = rtctime.epoch2cal(rtctime.get()+3*60*60)
	--print(string.format("%02d:%02d:%02d", tm.hour, tm.min, tm.sec))
	if tm.year ~= 1970 and dat.setH ~= 100 and dat.riseH ~= 100 then
		local tmnow = tm.hour * 60 + tm.min
		local nght = dat.setH * 60 + dat.setM
		local day = dat.riseH * 60 + dat.riseM
	
		if tmnow > nght and dat.now == 'Day' then
			dat.now = 'Night'
		elseif tmnow > day and dat.now == 'Night' then
			dat.now = 'Day'
		end
	else
		if dat.lux > 100 and dat.now == 'Night' then
			dat.now = 'Day'
		elseif  dat.lux < 5  and dat.now == 'Day' then
			dat.now = 'Night'
		end
		
	end
	print('Now is '..dat.now)
	
	if dat.powerON  then
		dat.powerON = nil
		dat.startwater = 0
		if dat.now == 'Day' then 
			dat.makedesDay = 1
		else
			dat.makedesNgt = 1
		end
	end
	tm, tmnow, nght, day = nil, nil, nil, nil

if dat.now == 'Day' and dat.makedesDay == 0 then
	dat.makedesNgt = 0
	if dat.lastDay >= dat.intervalDay then
		dat.lastDay = 0
		print('Water at Day!')
		dat.startwater = 1
	else
		dat.lastDay = dat.lastDay + 1
		print('Day Interval:', dat.lastDay)
		dofile('savedata.lua')
	end
	table.insert(topub, {'lastDay', dat.lastDay})
	dat.makedesDay = 1
end 

if dat.now == 'Night' and dat.makedesNgt == 0 then
	dat.makedesDay = 0
	if dat.lastNgt >= dat.intervalNgt then
		dat.lastNgt = 0
		print('Water at Night!')
		dat.startwater = 1
	else
		dat.lastNgt = dat.lastNgt + 1
		print('Night interval:', dat.lastNgt)
		dofile('savedata.lua')			
	end
	table.insert(topub, {'lastNgt', dat.lastNgt})
	dat.makedesNgt = 1
end 

if dat.startwater then
	dofile('turnvalve.lua')
end
