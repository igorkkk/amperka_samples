-- Параметр устанавлтвается через mqtt брокер
if dat.arm == "ON" then
	-- Гасим освещение
	dat.light = 'OFF'
	gpio.write(pins.light, gpio.LOW)
	print("Light set OFF")
	-- ГЛОБАЛЬНАЯ функция включения сирены
	function siren()
		-- Отмена наблюдения за ногой ПИР
		gpio.trig(pins.pir)
		print('Pir Disarmed')
		-- Включаем сирену
		dofile('sirennow.lua')
	end
	-- Назначаем вышеуказанную функцию как callback
	-- на ногу ПИР
	gpio.trig(pins.pir, "up", siren)
	print('Pir Armed')
-- Сняли охрану
elseif dat.arm == "OFF" then
	-- Снимаем наблюдение за ногой
	gpio.trig(pins.pir)
	print('Pir Disarmed')
	-- Выключаем сирену
	gpio.write(pins.siren, gpio.LOW)
	print('Siren stop')
	-- Уничтожаем функцию
	siren = nil
	-- Если работал таймер выключения сирены - уничтожаем.
	if dat.tmrsiren then
		dat.tmrsiren:stop()
		dat.tmrsiren:unregister()
		dat.tmrsiren = nil
	end
end