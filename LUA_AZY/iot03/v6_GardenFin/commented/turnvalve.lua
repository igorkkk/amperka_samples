--
do
	print('turnvalve works.')
	-- Этот файл просто так не вызывается! 
	-- Раз вызвали - прекращаем подачу питания на любые ноги крана, если она есть.
	if dat.vlpowered then
		gpio.write(pins.open, gpio.HIGH)
		gpio.write(pins.close, gpio.HIGH)
	end
	-- Если есть таймер подачи питания на ноги крана - убиваем
	if dat.powerpin then
		dat.powerpin:stop()
		dat.powerpin:unregister()
		dat.powerpin = nil
	end
	
	-- Функция подачи питания на ногу крана и удержания ее там 30 секунд
	local function runpin(pin)
        --Есть питание на ноге
		dat.vlpowered = true
		-- Подаем электричество
		gpio.write(pin, gpio.LOW)
		print('ON pin '..pin)
		-- Создаем таймер отключения питания
		dat.powerpin = tmr.create()
		dat.powerpin:register(30000, tmr.ALARM_SINGLE, function(tm) 
			-- Отключаем питание
			gpio.write(pin, gpio.HIGH)
			dat.vlpowered = nil
			tm = nil
			dat.powerpin = nil
			print("OFF by 30 sec. pin "..pin)
		end)
		-- Запуск таймера
		dat.powerpin:start()
	end
	
	-- При вызове файла проверяем зпись в таблице о том открывать или закрывать кран
	
	-- Если открывать
	if dat.startwater == 1 then
		-- Открываем
		runpin(pins.open)
		-- Оповещаем брокер
		table.insert(topub, {'iswatering',"ON", 1})
		
		-- Если не запущен таймер закрытия крана - создаем
		if not dat.tmrpoliv then 
			dat.tmrpoliv = tmr.create()
			-- Определяем когда закрыть
			dat.tmrpoliv:register(dat.poliv * 1000, tmr.ALARM_SINGLE, function(t) 
				-- Оповещаем брокер
				table.insert(topub, {'iswatering',"OFF", 1})
				-- Шлем питание на ногу 
				runpin(pins.close)
				t = nil
				dat.tmrpoliv = nl
			end)
			-- Запускаем таймер 
			dat.tmrpoliv:start()
		end
	
	end
	
	-- Если закрыть
	if dat.startwater == 0 then
		print("Got stop watering!")
		-- Если работает таймер полива - убиваем
		if dat.tmrpoliv then
			dat.tmrpoliv:stop()
			dat.tmrpoliv:unregister()
			dat.tmrpoliv = nil
		end
		-- Оповещаем брокер
		table.insert(topub, {'iswatering',"OFF", 1})
		-- Шлем питание на ногу закрытия крана
		runpin(pins.close)
	end
	dat.startwater = nil
end
