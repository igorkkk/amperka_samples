-- table.foreach(dat, print)

-- Функция вызова обработки файлов по таймеру

dispatch = function()
	print('\n')
	-- Флаг работы глдавной функции
	dat.dispatch = true
	-- Таблица запускаемых на исполнение файлов - корутин
	local threads = {}
	-- Перечень файлов, что надо последовательно выполнять
	local filestorun = {
		'getsun.lua',	-- (Для 1 и 3)Проверка времени восход/закат
		'lightnow.lua',	-- (1)Управление освещением
		'scedset.lua',	-- (2)Проверка расписания для отопителя
		'temper.lua',	-- (2)Управление отопителем
		'garden.lua',	-- (3)Полив
		'makepubl.lua'	-- Подготовка даных к публикации
	}
	-- Переменная для создания сопрограммы (корутины)
	local co
	-- Перебираем таблицу с именами файлов для выполнения 
	for _, v in ipairs(filestorun) do
		-- Создаем корутину для каждого имени файла
		co = coroutine.create(function ()
			-- Корутина не исполняет свой код до прямой команды на исполнение:
			return dofile(v)
		end)
		-- Вставляем задачу сопрограммы в таблицу для исполнения
		table.insert(threads, co)
	end
	-- Счетчик вызванных сопрограмм из таблицы
	local nextc = 1
	-- Функция вызова очередной сопрограммы
	local function ne()
		-- Если не достигли конца таблицы
		if nextc <= #threads then
			-- Вызываем корутину и одновременно печатаем результат ее вызова
			print('no '..nextc, filestorun[nextc] , (coroutine.resume(threads[nextc])))
			-- Увличиваем счетчик очередного вызова
			nextc = nextc + 1
		end
	end
	-- Старт вызова первой сопрограммы
	ne()
	-- Отальные сопрограммы вызываются через 3 секунды каждая
	tmr.create():alarm(3000, 1, function(t)
		if nextc <= #threads then 
			ne()
		else
			t:stop()
			t:unregister()
			t = nil
			nextc, ne, threads = nil, nil, nil
		end
	end)

end
-- Каждую минуту выполняем наши файлы 
tmr.create():alarm(60000, 1, dispatch)