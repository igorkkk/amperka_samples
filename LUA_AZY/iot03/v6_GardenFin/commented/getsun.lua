do
-- Часовой пояс Москвы
local offset = 3 

-- Здесь находится время для Москвы
-- 'api.sunrise-sunset.org/json?lat=55.75583&lng=37.61778'

-- Дергаем сервер раз в 10 минут. Можно еще реже 
if dat.setHMcount < 10 then
	dat.setHMcount = dat.setHMcount + 1
	print('Just:', dat.setH, dat.setM, dat.riseH, dat.riseM)
else	
	-- Локальные рабочие пермеменные
	local setH, setM, riseH, riseM, makeDig
	
	-- Текст запроса к серверу на тему времени
	local request = "GET /json?lat=55.75583&lng=37.61778&formatted=0 HTTP/1.1\r\n"..	 
		"Host: api.sunrise-sunset.org\r\n\r\n"
	-- Создаем соединение
	conn=net.createConnection(net.TCP, 0) 
	-- Соединились - запрос
	conn:on("connection", function(conn, payload) conn:send(request) end)
	-- Пришел ответ:
	conn:on("receive", function(conn, payload)
		-- Из тела ответа выгрызаем кусок со временем заката и рассвета
		local ttmm = string.sub(payload,string.find(payload,'sunrise')+20,string.find(payload,'sunset')+24)
		payload = nil
		--print(ttmm)
		-- Парсим время 
		setH, setM, riseH, riseM = string.match(ttmm,'(%d+):(%d+).+sunset.+T(%d+):(%d+)')
		--print(setH, setM, riseH, riseM)
		
		-- Функция переработки стрингов времени в цифры с учетом часового пояса 
		-- (переменная trans обработки часов)
		makeDig = function(dig, trans)
			dig = tonumber(dig)
			if trans then
				dig = ((dig + offset) < 24) and (dig + offset) or (dig - 21)
			end
			return dig
		end
		-- Устанавливаем время заката и рассвета
		if setH and setM then
			dat.setH = makeDig(setH, true)
			dat.setM = makeDig(setM)
		end
		if riseH and riseM then
			dat.riseH = makeDig(riseH, true)
			dat.riseM = makeDig(riseM)
		end
		
		-- Сброс счетчика проверки солнышка 
		dat.setHMcount =  0
		print('Set:', dat.setH, dat.setM, dat.riseH, dat.riseM)
		setH, setM, riseH, riseM, request, makeDig, ttmm, offset = nil, nil, nil, nil, nil, nil, nil, nil
		conn:close()
		end) 
	-- Присупаем к проверке времени	
	conn:connect(80, 'api.sunrise-sunset.org')
end
end