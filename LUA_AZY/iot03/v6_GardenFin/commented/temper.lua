-- Локальная таблица для сбора значений с датчиков
local temp = {}
-- Callback функция для передачи в модуль DS18b20,
-- ее вызовет модуль как только закончит опрос датчиков
local myWork = function(t)
	-- Выгружаем модуль	
	package.loaded["_ds18b20"]=nil
	ds = nil 
	-- Вставляем значения из временной таблицы в таблицу для публикации
	for k, v in pairs(temp) do
		table.insert(topub, {k,v})
		-- print(k,v)
	end
	-- Если в файле setglobals верно указан датчик контроля отопления,
	-- то есть его значение присутствует в таблице.
	if temp[dat.askds] then
		print('Now '..temp[dat.askds])
		-- Включаем или выключаем нагреватель и оповещаем таблицу dat
		if (temp[dat.askds] + 0.5) > dat.target then  gpio.write(pins.heat, gpio.LOW); dat.heat = 'OFF' end
		if (temp[dat.askds] - 0.5) < dat.target then  gpio.write(pins.heat, gpio.HIGH); dat.heat = 'ON' end
	else
		print('Set correct DS18b20 to be checked!')
	end
	
	temp, myWork = nil, nil	
end
-- Загрузка модуля
ds = require('_ds18b20')
-- Вызов чтения температуры
ds.getTemp(temp, myWork)