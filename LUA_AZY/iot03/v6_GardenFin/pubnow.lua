do
	dat.publ = true
	--[[
	for k,v in pairs(tohome) do
		table.insert(topub, {k, v})
	end
	--]]
	table.insert(topub, {'heap', node.heap()})
	local tp
	local strtpu = #topub
	--print('Start Pub  No:', strtpu)
	local function punow()
		if #topub ~= 0 and wifi.sta.getip() and dat.broker then
			-- print('pub: ', #topub)
			tp = table.remove(topub)
			tp[2] = tp[2] or ""
			tp[3] = tp[2] == "" and "1" or "0"
			m:publish(myClient.."/"..tp[1], tp[2], 2, tp[3], punow)
		else
			tp = nil
			dat.publ = false
			punow = nil
			dat.clpub = 0
			dat.dispatch = false
			print('Published!')
			return
		end
	end
	punow()
	-- Костыль
	---[[
	tmr.create():alarm(15000, 0, function(t)
		t = nil
		if dat.publ == true then
			print('----------------------- Kill Publishing! lost: ', #topub)
			if strtpu - #topub < 3 then
				dat.clpub = dat.clpub + 1
			end
			dat.dispatch = false
			topub = {}
			tp = nil
			dat.publ = false
			punow = nil
			if dat.clpub > 3 then
				print('Restart MQTT!')
				dat.clpub = 0
				m = nil
				dofile 'setmqtt.lua'
			end
		end
	end)
	--]]
end