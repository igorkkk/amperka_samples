do
pin=1 -- Нога для блинка
gpio.mode(pin, gpio.OUTPUT) -- Пин будет выходом
ligth = 1 -- Это будем писать в выход

function blink()
    -- все просто - пишем в выход
    gpio.write(pin, ligth)
    -- чтобы не плодить светодиодов
    -- дублируем в консоль что получается
    print("Write  to pin "..ligth)
    -- Это то же самое, что тренарный оператор в Сях
    ligth = (ligth == 0) and 1 or 0
end
-- вызываем функцию
blink()
end