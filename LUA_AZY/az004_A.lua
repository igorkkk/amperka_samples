do
function makeblink(pin)
    gpio.mode(pin, gpio.OUTPUT)
    local ligth = 1
    return function()
        gpio.write(pin, ligth)
        print("Writе  to "..pin.." "..ligth)
        ligth = (ligth == 0) and 1 or 0
    end
end

blink1 = makeblink(1)
blink2 = makeblink(2)

tmr.create():alarm(1000, tmr.ALARM_AUTO, blink1)
tmr.create():alarm(3000, tmr.ALARM_AUTO, blink2)
end