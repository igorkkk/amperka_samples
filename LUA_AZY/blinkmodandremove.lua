do
-- локальная переменная mm таким образом ловит свое собственное название
-- название модуля для дальнейшей выгрузки
local mm = ...
print(mm)
local M = {}
M.pinsintervals = {}

-- добавляем к функции, что изучали раньше, ссылку на внешнюю таблицу blinkstb
function M.makeblinkandtimer(pin, interval, blinkstb)
    gpio.mode(pin, gpio.OUTPUT)
    local ligth = 1
    local function blink()
        gpio.write(pin, ligth)
        print("Writе  to "..pin.." "..ligth)
        -- за одно будем смотреть свободную память
        print("Heap = "..node.heap())
        ligth = (ligth == 0) and 1 or 0
    end
    local function maketimer()
        -- поскольку нам нужны зацепки за таймер
        -- создаем его через переменную
        local ltmr = tmr.create()
        -- запускаем таймер
        ltmr:alarm(interval, tmr.ALARM_AUTO, blink)
        -- и помещаем ссылку на него в глобальную таблицу
        table.insert(blinkstb, ltmr)
    end
    return maketimer()
end

function M.make(t, blinkstb, call)
    M.pinsintervals = t
    for _, v in pairs(M.pinsintervals) do
		M.makeblinkandtimer(v[1], v[2], blinkstb)
    end
    -- call - callback функция, будет исполнена в завершение
    -- работы модуля
    if call then
        -- вызов callback функции
        call()
        -- уничтожение самого загруженного модуля
        package.loaded[mm] = nil
    end
end
return M
end
