do

function callbk(data)
    print("Now Start Callback Function!")
    print(string.format("%.02f", data))
end

function getrand(call)
    local sum = 0
    for i = 1, 5000 do
        sum = sum + math.random()
        --print(sum)
    end
    if call then call(sum)end
end

getrand(callbk)

end
