-- getmqtt.lua 
local M={}
function M.connecting(m, Broker, port, myCl, mod, unload)
    local getConnect
    getConnect = function()
       if mod.internet == true then
            m:connect(Broker, port, 0, 0,
            function(con)
                tmr.stop(getmq)
                tmr.unregister(getmq)
                getmq = nil
                print("Connected to "..Broker.." at "..port)
				_G.mod.broker = true
                m:publish("from"..myCl,"ON",0,0, function()
					m:subscribe({[myCl.."/hot"]=2,[myCl.."/cool"]=2}, function(conn)
						print("Subscribed.")
						if unload then
							getConnect = nil
							unload()
							package.loaded["getmqtt"]=nil
						end
					end)
				end)
            end)
        end
    end
    getmq = tmr.create()
    getmq:alarm(10000, 1, function()
        getConnect()
    end)
    dofile("getwifi.lua")
    getConnect()
end
return M
