if not water then water = {hot = 0, cool = 0} end
if not mod then mod = {} end

mtop = ""
mload = ""

myClient = "water02"
local pass = "superpassword"

m = mqtt.Client(myClient, 60, myClient, pass)

m:lwt(myClient, "OFF", 0, 0)

m:on("offline", function(con)
	m:close()
	mod.broker = false
	gotmqttnow()
end)

m:on("message", function(conn, topic, data)
	topic = string.gsub(topic, myClient.."/","")
	mtop = topic
	mload = data
	print(mtop, mload)
	dofile("analize_broker.lua")
end)

m:connect("iot.eclipse.org", 1883, 0, 0,
	function(con)
		print("Connected to Broker")
		m:subscribe({[myClient.."/cool"]=0,[myClient.."/hot"]=0}, function(conn)
			print("Subscribed.")
		end)
		m:publish(myClient, "ON", 0,0)
		mod.broker = true
end)
