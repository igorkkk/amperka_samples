-- main.lua
mod = {}

water = {
    cool = 0,
    hot = 0,
}

sent = {
    cool = 0,
    hot = 0,
}
dofile("data.lua")

deb = require("debm")
function doshort3()
    water.cool = water.cool + 0.01
    print("Cool got "..water.cool.." m3.")
end

function doshort4()
    water.hot = water.hot + 0.01
    print("Hot got "..water.hot.." m3.")
end
deb.set(3, doshort3)
deb.set(4, doshort4)

function gotmqttnow()
	wf = require("getwifi")
	local call = function()
		dofile("workmqtt.lua")
		wf = nil
		print("wf = ", wf)
	end
	wf.getwifi(call)
end
gotmqttnow()

tmr.create():alarm(30000, 1,  function()
	if (water.cool ~= sent.cool) or (water.hot ~= sent.hot) then
		dofile("pubwater.lua")
	else
		print("No changes!")
	end
end)


