-- works
local function setnew (pin)
	return {}
end
local M = {}
M.set = function(pin, short)
gpio.mode(pin, gpio.INPUT, gpio.PULLUP)
local o = setnew ()
o.buttonPin = pin
o.cicle = 0
o.gotpress = false
o.doshort = short
o.catch = 0
o.startpin = function(self) 
	gpio.trig(self.buttonPin)
	gpio.trig(self.buttonPin, "down",function (level)	
		if self.gotpress == false then
			self.gotpress = true
			local function exitnow(buf)
				tmr.stop(buf) 
				tmr.unregister( buf)
				buf = nil
				self.doshort()
				self.cicle, self.gotpress = 0, false 
			end
			tmr.create():alarm(50, 1, function(buf)
				if gpio.read(self.buttonPin) == o.catch then
					self.cicle = self.cicle + 1
				end
				if self.cicle >=5 then
					if o.catch == 0 then 
						self.cicle = -15
						o.catch = 1
					else
						self.cicle = 0
						o.catch = 0
						exitnow(buf)
					end
				end
			end)
		end
	end)
end
return o:startpin()
end
return M
