if mod.broker == true then
	dofile("savedata.lua")
	if not water then water = {} end
	if not sent then sent = {} end
	dofile("savedata.lua")
	local topub = {}
	for k,v in pairs(water) do
		table.insert(topub, {k,v})
		sent[k] = v
	end

	local function punow()
		if #topub ~= 0 then
			local tp = table.remove(topub)
			local top = "water02".."/"..tp[1].."/state"
			local dat = tp[2]
			m:publish(top, dat, 0,0, function(cl) 
				print(top, dat)
				punow()
			end)
		end
	end
	punow()
end
