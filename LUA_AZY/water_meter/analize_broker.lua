---[[
if not water then water = {} end 
myClient = "water02" 
--]]

local data = tonumber(mload) or 0
local top = mtop
mtop = ""
mload = ""

if data ~= 0 then
	if top == "cool" then 
		water.cool =  data
		print("Set water.cool to "..water.cool)
	end
	if top == "hot" then  
		water.hot = data
		print("Set water.hot to "..water.hot)
	end
	m:publish(myClient.."/"..top, "", 0, 1)
end
