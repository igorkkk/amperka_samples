--[[
works module:

local call = function()
    print("Got WiFi!")
    wf = nil
end
wf = require("_getwifi")
wf.check(call)
--]]

local modn = ...
local M = {}
M.counter = 1
M.check = function(call)
    local isconnect = function()
        tmr.create():alarm(15000, tmr.ALARM_SINGLE,
        function(t)
            t = nil
            M.check(call)
        end)
    end

    local ip = wifi.sta.getip()
    if ip ~= nil then
        net.dns.resolve("www.google.com", function(sk, ip)
            if (ip == nil) then 
                M.counter = M.counter + 1
				print("DNS fail: Ask Connect!")
				if M.counter == 5 then node.restart() end 
                isconnect()
            else 
                print('Got google: '..ip)
                if not mod then mod = {} end
                mod.internet = true
                if call then call() end
                package.loaded[modn] = nil
            end
        end)
    else
        isconnect()
    end
end
return M
 
