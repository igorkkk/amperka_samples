do
local M = {}
M.pinsintervals = {}

function M.makeblinkandtimer(pin, interval)
    gpio.mode(pin, gpio.OUTPUT)
    local ligth = 1
    local function blink()
        gpio.write(pin, ligth)
        print("Writе  to "..pin.." "..ligth)
        ligth = (ligth == 0) and 1 or 0
    end
    local function maketimer()
        tmr.create():alarm(interval, tmr.ALARM_AUTO, blink)
    end
    return maketimer()
end

function M.make(t)
    M.pinsintervals = t
    for _, v in pairs(M.pinsintervals) do
        M.makeblinkandtimer(v[1], v[2])
    end
end
return M
end