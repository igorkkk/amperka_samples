local M={}
M.adrtbl = {}
M.pin = 4

function M.addrs()
    ow.setup(M.pin)
    ow.reset_search(M.pin)
    local adr
    repeat
        adr = ow.search(M.pin)
        if(adr ~= nil) then
            table.insert(M.adrtbl, adr)
        end
    until (adr == nil)
    ow.reset_search(M.pin)
    adr = nil
    M.askT()
end

function M.askT()
    ow.reset(M.pin)
    ow.write(M.pin, 0xcc, 1)
	ow.write(M.pin, 0x44, 1)
	ow.reset(M.pin)
end

function M.readResult(temptabl)    
    local data
    local crc
    local t
    for _, v in pairs(M.adrtbl) do
        ow.reset(M.pin)
        ow.select(M.pin, v)
        ow.write(M.pin,0xBE,1)
        local data = ow.read_bytes(M.pin, 9)
        if (ow.crc8(string.sub(data,1,8)) == data:byte(9)) then
            t = (data:byte(1) + data:byte(2) * 256)
            if (t > 32767) then t = t - 65536 end
            t = t * 625 /10000
            local as = ""
            for ii = 1, #v do
                local hx = string.format("%02X", (string.byte(v, ii)))
                as = as ..hx
            end
            -- temptabl[as] = t
            temptabl[as] = string.format("%.2f",t)
            
        end
    end
    data = nil
    crc = nil
    t = nil
end

function M.getTemp(ttable, call, pin, del)
    M.pin = pin or M.pin
    if #M.adrtbl == 0 then
        M.addrs()
    else
        M.askT()
    end
    tmr.create():alarm(del, tmr.ALARM_SINGLE, function (t) 
        t:unregister()
        t = nil
        M.readResult(ttable)
        if call then call(ttable) end
    end)
end
return M
