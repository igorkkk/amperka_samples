1. WiFi. [code=lua][/code]
ESP-8266 может трудиться как точка доступа, как клиент и как то и другое вместе.
Начинающие любят создавать именно точку доступа, завешивать на модуль http сервер, делать странички и через них общаться с модулем.
Это невероятно круто, но как только устройстd в вашем арсенале становится более трех, разгуливание по точкам доступа и их уникальным страничкам радосто не доставляет.

Поэтому, я работаю только с клиентом.
wifi на модуле устанавливается только единожды, после чего включенный модуль устанавливает и восстанавливает соединение самостоятельно.

В снипеты ESPlorer обавьте такой код и забудьте об установках wifi:

[code=lua]do
wifi.setmode(wifi.STATION)
wifi.sta.clearconfig()
local scfg = {}
scfg.auto = true
scfg.save = true
scfg.ssid = 'ТочкаДоступа'
scfg.pwd = 'ВашПароль'
wifi.sta.config(scfg)
wifi.sta.connect()
tmr.create():alarm(15000, tmr.ALARM_SINGLE, function() print('\n', wifi.sta.getip()) end)
end[/code]

2. init.lua

Файл init.lua запускается системой при старте. Если его нет - ничего не происходит. Есть - файл будет выполнен.
Некоторые юзеры заталкивают в него весь рабочий скрипт, но опыт показывает, что это не лучшее решение. 
Считаю, что этот файл должен вызывать на исполнение следующий рабочий файл, а сам обеспечивать лишь удобную отладку всей программы.
Предлагаю иметь пару файлов, обеспечивающих удобную работу со скриптами:

init.lua:
[code=lua]-- Файл для последующего запуска
local runfile = "setglobals.lua"
-- Если файла нет - переименовываем init.lua
-- чтобы не войти в бесконечный цикл перезагрузки
tmr.create():alarm(5000, 0, function()
    if file.exists(runfile) then
        dofile(runfile)
    else
        print("No ".. runfile..", Rename init.lua!")
        if file.exists("init.lua") then
            file.rename("init.lua","_init.lua")
            node.restart()
        end
    end
end)[/code]

rename.lua.
Файл переименовывает init.lua в _init.lua и обратно:
[code=lua]if file.exists("init.lua") then
     file.rename("init.lua","_init.lua")
     node.restart()
elseif file.exists("_init.lua") then
     print("Really rename to init.lua? \n 10 sec. delay!")
     tmr.create():alarm(10000, 0, function() 
          file.rename("_init.lua","init.lua")
          node.restart()
     end)
end[/code]

3. setmqtt.lua

Этот файл задает параметры и устанавливает соединение с mqtt брокером. 
Протокол mqtt - вещь страшная, непонятная, запутанная. Нормальный ардуинщик постичь его не в состоянии.

Гораздо проще поднять какой-нибудь сервак, замутить на нем странички, с которыми работать POST запросами.
Можно раскрасить его CSS и тому подобной тривиальщиной. 

Или, все-таки, MQTT.
Клиент шлет простые сообщения на брокер. 
Сообщение состоит из 
- топика (признак, определение сообщения); 
- данных;
- специальных флагов.

Брокер ловит сообщения и пересылает подписчику.
Любое устройство может быть одновременно клиентом и подписчиком.

Нюансы.
- Сообщения, по-умолчанию, получают подписчики, которые в момент его прихода находятся на связи с брокером.
Чтобы сообщения получали и "вновь прибывшие" подписчики (например не было связи в этот момент) - следует установить в сообщении специальный флаг.  

- "Последняя воля" - специальное сообщение, которое отправляеся всем подписчикам, когда клиент потерял связь с брокером.
Полагаю, для начала это все что еужно знать о MQTT.

Вы можете поднять свой брокер, воспользоваться платным или общедоступным брокером.
Наиболее известная халява - "iot.eclipse.org". Если данные вашего термометра составляют государственную тайну - воспользуйтесь платным брокером или поднимайте свой.

3.1. Параметры.
Создаем объект - соединение MQTT.
[code=lua]m = mqtt.Client( "myClient", 60, "user", "password")[/code]

Собщение "Последняя воля", пусть будет так:
[code=lua]m:lwt('myClient'..'/state', 'OFF', 0, 0)[/code]

Далее определяем callback(и) для двух событий,  
получение сообщения от брокера:

[code=lua]m:on("message", function(conn, topic, dt)
	local top = string.gsub(topic, myClient.."/command/","") 
	print(top, ":", dt) 
	if dt then
	   table.insert(killtop, {top, dt})
	   dofile("analize.lua") 
	end
end)[/code]

потеря связи с брокером:

[code=lua]m:on("offline", function(con)
	dat.broker = false
	dofile('setmqtt.lua')
end)[/code]

... Продолжение позже 



3.2. Установка соединения.

Соединение установим следующим скриптом:

[code=lua]-- (Сервер, порт, два неважных пока параметра,
-- и два callback)
m:connect('iot.eclipse.org', 1883, 0, 0,
	-- callback на успешное соединение
	function(con)
		print("Connected to Broker")
		-- Подписываемся на получение данных от топика
		-- 'myClient/command/'
		m:subscribe("myClient".."/command/#",0, function(conn)
			print("Subscribed.")
		end)
		-- Публикем информацию о подключении в топик
		-- 'myClient/state'
		m:publish('myClient'..'/state',"ON",0,0)
		-- Выставляем флаг соединения с брокером
		dat.broker = true
	end,
	-- callback на невозможность соединения
	function(client, reason)
		print("failed mqtt: " .. reason)
		-- Вызываем себя рекурсивно
		dofile('setmqtt.lua')
end)[/code]


Таким образом:
[code=lua]print('Set MQTT')
-- Если раньше не завели таблицу 'dat' для рабочих параметров - заводим
if not dat then dat = {} end
-- 'myClient' - переменная, если ее нет - будет "test001" 
if not myClient then myClient = "test001" end
-- 'killtop' - таблица для приема полученных команд через mqtt
-- и отправки на обработку в файл 'analize.lua'
if not killtop then killtop = {} end
if not m then
	m = mqtt.Client( myClient, 60, myClient, 'passfor'..myClient)
	m:lwt(myClient..'/state', "OFF", 0, 0)
	m:on("message", function(conn, topic, dt)
		-- очищаем пришедший от брокера топик до чистого топика-команды
		-- приходить они будут в формате 'myClient/command/setTemperature'
		-- "очищенная" и передаваемая для анализа - 'setTemperature'		
		local top = string.gsub(topic, myClient.."/command/","") 
		print(top, ":", dt) 
		if dt then 
			-- заполняем таблицу killtop данными формата
			-- '{'setTemperature', '25.5'}'
			table.insert(killtop, {top, dt})
			-- отправляем для анализа если такой анализ
			-- еще не производится (разберемся позже)
			if not dat.analiz then
				dofile("analize.lua") 
			end
		end
	end)
	m:on("offline", function(con)
		dat.broker = false
		dofile('setmqtt.lua')
	end)
end
-- Если файл вызывается рекурсивно 
-- соединение надо принудительно остановить
m:close()
-- Счетчик ожидания wifi, нужен для некоторых случаев
local count = 0
local connecting = function(getmq)
	-- Проверяем wifi
	if wifi.sta.status() == 5 then
		print('Got wifi')
		tmr.stop(getmq)
		tmr.unregister(getmq)
		getmq = nil
		print('iot.eclipse.org!!!!!!!')
		m:connect('iot.eclipse.org', 1883, 0, 0,
		function(con)
			print("Connected to Broker")
			m:subscribe(myClient.."/command/#",0, function(conn)
				print("Subscribed.")
			end)
			m:publish(myClient..'/state',"ON",0,0)
			dat.broker = true
			count = nil
		end,
		function(con, reason)
			print("failed mqtt: " .. reason)
			dofile('setmqtt.lua')
		end)
	else
		print("Wating for WiFi "..count.." times")
		count = count + 1
		-- if count > 20 then node.restart() end
	end
end
-- Таймер периодически запускает функцию соединения
tmr.create():alarm(5000, 1, function(t)
	-- и передает в нее свои опознавательные знаки
	-- чтобы его остановить и "убить"
	connecting(t)
end)[/code]


