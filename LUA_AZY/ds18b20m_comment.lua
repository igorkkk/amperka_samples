local M={}
M.adrtbl = {} -- склад адресов датчиков
M.pin = 4 -- нога датчиков по умолчанию
M.del = 750 -- интервал чтения температуры по умолчанию
function M.getaddrs(ttable, call)
    -- запуск one wire
	ow.setup(M.pin)
    ow.reset_search(M.pin)
    -- в цикле ищем все датчики
	repeat
        local adr = ow.search(M.pin)
        if(adr ~= nil) then
            -- заталкиваем адреса в таблицу
			table.insert(M.adrtbl, adr)
        end
    until (adr == nil)
	-- стряхиваем поиск с шины 
    ow.reset_search(M.pin)
	-- вызываем функцию запроса температуры
	-- и пинаем ей таблицу и callback функцию
	-- что приняли при вызове 
    M.askTemp(ttable, call)
end
function M.askTemp(ttable, call)
	-- так нужно о даташиту
	ow.setup(M.pin)
	-- в цикле перебираем все адреса из таблицы
	-- и задаем пинка всем датчикам: "Читай температуру!"
    for _, v in pairs(M.adrtbl) do
        ow.reset(M.pin)
        ow.select(M.pin, v)
        ow.write(M.pin, 0x44, 1)
    end
	-- создаем таймер, и через M.del мс вызываем функцию чтения
	-- ячеек датчика.
	-- передаем транзитом ttable, call - в этой функции они нам 
	-- тоже не нужны
	tmr.create():alarm(M.del, tmr.ALARM_SINGLE, function (t) 
		M.readResult(ttable, call)
		-- уничтожаем этот таймер за ненадобностью
		t = nil
	end)
end
function M.readResult(ttable, call)    
    -- так надо для обработки данных:
	local data, crc, t
	-- в цикле читаем и обрабатываем ячейки датчиков 
    for _, v in pairs(M.adrtbl) do
        ow.reset(M.pin)
        ow.select(M.pin, v)
        ow.write(M.pin,0xBE,1)
        data = string.char(ow.read(M.pin))
        for i = 1, 8 do
            data = data .. string.char(ow.read(M.pin))
        end
        crc = ow.crc8(string.sub(data,1,8))
        if (crc == data:byte(9)) then
            t = (data:byte(1) + data:byte(2) * 256)
            if (t > 32767) then t = t - 65536 end
            t = t * 625 /10000
			-- полученный результат толкаем в таблицу
			-- наконец она нам пригодилась
            table.insert(ttable, t)
        end
    end
	-- как прочитали все датчики и заполнили таблицу
	-- вызываем callback функцию и передаем ей данные
	if call then call(ttable) end
end
function M.getTemp(ttable, call, pin, del)
	-- если таблица с адресами датчиков пуста
	-- значит это первый вызов(или ошибка), надо вызвать
	-- функцию отлова адресов
	if #M.adrtbl == 0 then
		-- уточняем ногу, где сидят датчики 
		M.pin = pin or M.pin
		-- уточняем задержку между запросом и чтением
		M.del = del or M.del
		-- вызываем функцию запроса адресов
		-- которая дальше все равно вызовет запрос температуры
		M.getaddrs(ttable, call)
	else
	-- если адреса датчиков есть - запрос температуры
		M.askTemp(ttable, call)
	end
end
return M

