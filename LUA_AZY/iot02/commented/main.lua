-- Каждые 2 минуты публикуем данные о состоянии устройства
tmr.create():alarm(120000, 1, function()
	print('Time for Publish!')
	-- Если публикация возможна
	print('dat.broker = ', dat.broker)
	if dat.broker then
		-- Толкаем в таблицу для публикации данные
		table.insert(topub, {'target', dat.target})
		table.insert(topub, {'light', dat.light})
		-- Если публикация в данный момент не идет - запускаем
		print('dat.publ = ', dat.publ)
		if not dat.publ then dofile('pubnow.lua') end
	end
end)