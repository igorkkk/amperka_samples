dat.analiz = true
if #killtop ~= 0 then
	local com = table.remove(killtop)
	local top = com[1]
	local dt = com[2]
	if top and dt then
		if top == "light" then
			if dt == "ON" then
				dat.light = "ON"
				gpio.write(pinlight, gpio.HIGH)
				print("Light is ON")
			end
			if dt == "OFF" then
				dat.light = "OFF"
				gpio.write(pinlight, gpio.LOW)
				print("Light is OFF")
			end
		elseif top == "target" then 
			local target = tonumber(dt) or 75
			if target == 75 then
				print('Got Wrong Target!')
			else
				if target < 15 then 
					dat.target = 15
				elseif target > 27 then 
					dat.target = 27
				else
					dat.target = target
				end
			end
			target = nil
			print('Set Target Temperature At '..dat.target)
		end
		if not dat.publ then dofile('pbnow.lua') end
	end	
	com, top, dt = nil, nil, nil
	dofile('analize.lua')
else
	dat.analiz = nil
end
