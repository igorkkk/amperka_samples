do
pinsintervals = {
    {1, 1000},
    {2, 3000}
}

function makeblinkandtimer(pin, interval)
    gpio.mode(pin, gpio.OUTPUT)
    local ligth = 1
    function blink()
        gpio.write(pin, ligth)
        print("Writе  to "..pin.." "..ligth)
        ligth = (ligth == 0) and 1 or 0
    end
    function maketimer()
        tmr.create():alarm(interval, tmr.ALARM_AUTO, blink)
    end
    return maketimer()
end

for _, v in pairs(pinsintervals) do
    makeblinkandtimer(v[1], v[2])
end
end