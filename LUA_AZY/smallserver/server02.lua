do
    srv = net.createServer(net.TCP)
    function receiver(sck, data)
        local function closec()
            sck:close()
        end
        local function send()
           sck:send("<h1> Hello, NodeMCU!!!</h1>")
        end
        sck:on("sent", closec)
        send()
    end
    srv:listen(80, function(conn)
      conn:on("receive", receiver)
    end)
end