do
dat = {
    temp = 27.4,
    -- humi = "Не знаю, но там сухо!"
    humi = "No Info!"
}
local function expand (s)
    return (string.gsub(s, "$(%w+)", dat))
end
srv = net.createServer(net.TCP, 15)
local function receiver(sck, data)
    data = nil
    local function closec()
        sck:close()
		sck = nil
		closec = nil
    end
    local function send()
        if file.open("page.lua", "r") then
            repeat
                local line = file.readline()
                if line then
                    line = expand(line)
                    sck:send(line)
                end
            until line == nil
            file.close()
            file = nil
			line = nil
        else
          sck:close()
		  sck = nil
        end
        print('on sent:', node.heap())
    end
    sck:on("sent", closec)
    send()
end
srv:listen(80, function(conn)
  conn:on("receive", receiver)
end)
tmr.create():alarm(30000, 1, function() print(node.heap()) end) 
print(node.heap())
end