do
-- Создаем сервер
srv = net.createServer(net.TCP)

-- Даем серверу команду слушать 80 порт
-- Как только происходит соединение
-- вызывается анонимная функция с передачей 
-- ей параметра соединения "conn"
srv:listen(80, function(conn)
    -- Функция готовит реакцию на два события "conn"
	-- По получению данных от клиента - печать и отправка
	conn:on("receive", function (sck, data)
        print(data)
        conn:send("<h1> Hello, NodeMCU!!! </h1>")
    end)
	-- По отправке - рассоединение
    conn:on("sent", function() 
         conn:close()
    end)
end)
end