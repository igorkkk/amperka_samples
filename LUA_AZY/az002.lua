do
pin=1 -- Нога для блинка
gpio.mode(pin, gpio.OUTPUT) -- Пин будет выходом
ligth = 1 -- Это будем писать в выход

function blink()
    -- все просто - пишем в выход
    gpio.write(pin, ligth)
    -- чтобы не плодить светодиодов
    -- дублируем в консоль что получается
    print("Writе  to pin "..ligth)
    -- Этото же самое, что тренарный оператор в Сях
    ligth = (ligth == 0) and 1 or 0
end
-- вызываем функцию
blink()

-- Создаем таймер
tmrBlink = tmr.create()
-- Интервал подмигивания
interval = 5000
-- как работает таймер, смотрим документацию:
mode = tmr.ALARM_AUTO
-- запускаем таймер
tmrBlink:alarm(interval, mode, blink)
end