do
-- глобальная таблица - склад таймеров-блинков
blinkstb = {}
pinsintervals = {
    {1, 5000},
    {2, 3000},
    {3, 500}
}

-- callback функция:
-- уничтожаем в памяти следы
-- загрузки модуля и глобальной
-- таблицы с ногами и интервалами

function killmod()
    pinsintervals = nil
    bl = nil
end

-- грузим модуль
bl = require('blinkmodandremove')
-- передаем в него данные о ногах и интервалах,
-- о таблице - складе таймеров-блинков
-- о функции, которая очистит память от лишнего

bl.make(pinsintervals, blinkstb, killmod)

-- через 10 секунд таймер уничтожит все блинки
tmr.create():alarm(10000, 0, function(t)
    print("Stop And Kill Blinks!")
    --[[
    tmr.stop(blinkstb[1])
    tmr.stop(blinkstb[2])
    tmr.unregister(blinkstb[1])
    tmr.unregister(blinkstb[2])
    blinkstb[1] = nil
    blinkstb[2] = nil
    blinkstb = nil
    -- и уничтожит сам себя
    t = nil
   --]] 
   ---[[ и то же самое компактно
    -- для любого количества блинкеров
    for _, v in pairs(blinkstb) do
          tmr.stop(v)
          tmr.unregister(v)
     end
     blinkstb = nil
     t = nil
    --]]

end)
end
