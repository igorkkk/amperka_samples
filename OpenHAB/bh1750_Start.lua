--
sda = 1
scl = 2
bh1750 = require("bh1750")
bh1750.init(sda, scl)

function printlux(l)
    l = string.format("%.2f", l/100)
    print("Lux = "..l.." lx")
end

tmr.alarm(0, 10000, 1, function()
    bh1750.getlux(printlux)
end)

