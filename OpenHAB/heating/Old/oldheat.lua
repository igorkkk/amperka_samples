pin=4 -- На этой ноге DS18b20
gpio.mode(3,gpio.OUTPUT) -- Здесь реле
gpio.write(3,gpio.LOW) -- Ногу вниз - реле включить

Broker="iksite.selfip.org"
port=1883
myClient="ESPSKOTEL"
name="kotel"
pass="password"

function readTGet()
  file.open("data.txt","r")
  data = file.read()
  file.close()
  return tonumber(data)
end
function writeTGet(d)
  file.open("data.txt", "w+")
  file.write(d)
  file.close()
end

tGet=readTGet ()
print("Get: "..tGet)

m = mqtt.Client(myClient, 180, name, pass)
m:lwt("/lwt", "ESPSKOTEL", 0, 0)
m:on("offline", function(con)
   print ("reconnecting...")
  tmr.alarm(1, 90000, 1, function()
     m:connect(Broker, port, 0, function(conn)
       tmr.stop(1)
       m:subscribe("/myhome/dacha/kotel/command",0, function(conn)
         print ("Subscribed!")
       end)
     end)
  collectgarbage()
  end)
end)

tmr.alarm(2, 60000, 1, function()
  ds18b20 = require('ds18b20')
  ds18b20.setup(pin) -- На этой ноге DS18b20
  addres={}
  addres=ds18b20.addrs()
  local t = ds18b20.read(addres[1])
  ds18b20 = nil
  package.loaded["ds18b20"]=nil
  if t ~= 85 then
  if wifi.sta.status() == 5 then
       m:publish("/myhome/dacha/kotel/state",t,0,0, function(conn)
         print("Temp "..t.." and "..node.heap().." published!")
       end)
     end
     print("Get "..tGet)
  if t < (tGet-0.5) then
  gpio.write(3,gpio.LOW)
  end
  if t > (tGet+0.5) then
       gpio.write(3,gpio.HIGH)
  end
  end
  collectgarbage()
  print(node.heap())
end)

m:on("message", function(conn, topic, data)
  print("Recieved:" .. topic .. ":" .. data)
  data = tonumber(data)
   if (data > 15) and (data < 28) then
     tGet=data
     writeTGet(tGet)
     tmr.stop(2)
     m:publish("/myhome/dacha/kotel/get",tGet,0,0,
     function(conn)
       tmr.start(2)
     end)
   else
     print("Invalid - Ignoring")
   end
   collectgarbage()
end)

tmr.alarm(0, 1000, 1, function()
   if wifi.sta.status() == 5 and wifi.sta.getip() ~= nil then
     tmr.stop(0)
     m:connect(Broker, port, 0, function(conn)
       print("Connected!")
       m:subscribe("/myhome/dacha/kotel/command",0, function(conn)
         print("Subscribed!")
       end)
     end)
   end
end)