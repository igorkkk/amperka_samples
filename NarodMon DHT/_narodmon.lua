local M={}
function M.sendNarod(MACaddr, datatbl, call)
    if type(datatbl) ~= "table"  then 
        print("No table temperature!")
        return
    end
    for k, v in pairs(datatbl) do
        MACaddr = MACaddr.."#"..k.."#"..v.."\n"
    end
    MACaddr = MACaddr.."##\n"
    print(MACaddr)
    conn=net.createConnection(net.TCP, 0)
    conn:on("connection",function(conn, payload)
        conn:send(MACaddr)
    end)
    conn:on("receive", function(conn, payload)
        print('\nRetrieved in '..(string.format("%02.2f", (tmr.now()-t)/1000000)..' seconds.'))
        print('Narodmon says '..payload)
        t = nil
        conn:close()
        -- conn = nil
        if call then
           conn = nil
           call()
        end
    end)
    t = tmr.now()
    conn:connect(8283,'narodmon.ru')
end
return M


