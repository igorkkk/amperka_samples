do
------------ Set yours ---------
-- Set DHT pin:
local pin = 4
-- Set New MAC address:
local MACaddr = "#12-34-56-78-90-AB\n"
-- Set numbers of trying to get good status DHT-11
local bingo = 20
-- Set in minutes interval to send data to Narodmon
local interval = 5
--------------------------------
local function removenarod()
    narod = nil
    package.loaded["_narodmon"]=nil
    conn = nil
end

local function sendnarod(ttable)
    ttable['heap'] = node.heap()
    if not wifi.sta.getip() then print("No wifi!"); return end
    narod = require('_narodmon')
    narod.sendNarod(MACaddr, ttable, removenarod)
end

local now = function()
    local lbingo = bingo
    local askDHT
    askDHT = function()
        lbingo = lbingo - 1
        local status, temp, humi = dht.read11(pin)
        local ttable = {}
        if status == dht.OK then
           temp = string.format("%.2f",temp)
           humi = string.format("%.2f", humi)
           print("DHT Temperature:"..temp..";".."Humidity:"..humi)
           ttable.temp = temp
           ttable.humi = humi
           ttable.bingo = bingo - lbingo     
        elseif status == dht.ERROR_CHECKSUM then
            print( "Bingo: "..(bingo - lbingo).." DHT Checksum error." )
        elseif status == dht.ERROR_TIMEOUT then
            print( "Bingo: "..(bingo - lbingo).." DHT timed out." )
        end
        if status == dht.OK or lbingo == 0 then
            sendnarod(ttable)
        else        
            tmr.create():alarm(5000, 0, function() 
				this = nil
				askDHT()
			end)
        end
        
    end 
    askDHT()    
end
now()
tmr.create():alarm(interval*60*1000, tmr.ALARM_AUTO, now)
end
